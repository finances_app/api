module.exports = {
    API_ADDRESS: 'https://localhost',
    INSTANCE: 'finances_dev',
    DATA: {
        MONGODB_HOST: 'mongodb://localhost/',
        ELASTICSEARCH_HOST: 'http://localhost:9200',
    },
    SYSTEM_EMAILS: {
        ADMIN: '',
        MAILER: '',
    },
    IMPORT: {
        parsing_encoding: 'latin1',
        parsing_delimiter: ',',
        category_es_score_threshold: 50,
        desjardins: {
            columns_type: {
                0: 'string',
                1: 'number',
                2: 'string',
                3: 'string',
                4: 'number',
                5: 'string',
            },
            minimum_line_count: 3,
            column_count: 14,
        },
        visa: {
            columns_type: {
                0: 'string',
                3: 'string',
                4: 'number',
                5: 'string',
            },
            minimum_line_count: 1,
            column_count: 14,
            date_separator: '/',
            date_format: 'yyyy/MM/dd',
        },
        paypal: {
            columns_type: {
                0: 'string',
                1: 'string',
                2: 'string',
                4: 'string',
                5: 'string',
                6: 'string',
            },
            minimum_line_count: 3,
            column_count: 10,
            decimal_char: '.',
            thousand_separator: ',',
            date_format: 'dd/MM/yyyy',
            date_separator: '/',
            transaction_status: {
                completed: ['Completed', 'Partially Refunded', 'Refunded', 'Reversed'],
                pending: ['Pending'],
                refunded: ['Refunded'],
                reversed: ['Reversed'],
                placed: ['Placed'],
                removed: ['Removed'],
            },
            transaction_type: {
                purchase: [
                    'Preapproved Payment Sent',
                    'Express Checkout Payment Sent',
                    'Mobile Express Checkout Payment Sent',
                    'Web Accept Payment Sent',
                    'Website Payment',
                    'Payment Sent',
                    'Shopping Cart Payment Sent',
                    'Subscription Payment Sent',
                    'PreApproved Payment Bill User Payment',
                    'eBay Auction Payment',
                    'Postage Payment',
                    'Express Checkout Payment',
                ],
                paymentReceived: ['Express Checkout Payment Received'],
                currencyConversion: ['Currency Conversion', 'General Currency Conversion'],
                bankAccount: [
                    'Add Funds from a Bank Account',
                    'Withdraw Funds to Bank Account',
                    'Bank Deposit to PP Account ',
                    'General Withdrawal',
                ],
                accountDeposit: [
                    'Add Funds from a Bank Account',
                    'Bank Deposit to PP Account ',
                    'Bank Deposit to PP Account',
                ],
                accountWithdraw: ['Withdraw Funds to Bank Account', 'General Withdrawal'],
                refund: ['Refund', 'Payment Refund', 'Reversal'],
                fundsRelease: ['External Release'],
                reversal: ['Payment Reversal'],
                accountHold: [
                    'Account Hold for ACH deposit',
                    'Reversal of General Account Hold',
                    'Account Hold for Open Authorization',
                ],
                authorization: ['Authorization', 'Order', 'Update to Add Funds from a Bank Account'],
            },
        },
    },
    ELASTICSEARCH: {
        settings: {
            max_ngram_diff: 30,
            analysis: {
                analyzer: {
                    ngram_analyser_1: {
                        type: 'custom',
                        tokenizer: 'ngram_tokenizer',
                        filter: ['lowercase'],
                    },
                },
                tokenizer: {
                    ngram_tokenizer: {
                        type: 'ngram',
                        min_gram: 3,
                        max_gram: 3,
                        token_chars: ['letter', 'digit'],
                    },
                },
            },
        },
        mappings: {
            properties: {
                categories: {
                    type: 'text',
                    fields: {
                        ngram: {
                            type: 'text',
                            analyzer: 'ngram_analyser_1',
                        },
                        raw: {
                            type: 'keyword',
                        },
                    },
                },
                category: { type: 'keyword' },
                amount: { type: 'double' },
                accountType: { type: 'keyword' },
                conversionRate: { type: 'double' },
                creationDate: { type: 'date' },
                currency: { type: 'keyword' },
                date: { type: 'date' },
                type: { type: 'keyword' },
                description: {
                    type: 'text',
                    fields: {
                        ngram: {
                            type: 'text',
                            analyzer: 'ngram_analyser_1',
                        },
                        raw: {
                            type: 'keyword',
                        },
                    },
                },
                descriptionMain: {
                    type: 'text',
                    fields: {
                        ngram: {
                            type: 'text',
                            analyzer: 'ngram_analyser_1',
                        },
                        raw: {
                            type: 'keyword',
                        },
                    },
                },
                location: { type: 'keyword' },
                statement: { type: 'keyword' },
                ignoreForAutoCategoryAssignment: { type: 'boolean' },
            },
        },
    },
};
