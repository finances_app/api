module.exports = {
    API_ADDRESS: 'http://127.0.0.1:9091',
    INSTANCE: 'finances_ci',
    DATA: {
        MONGODB_HOST: 'mongodb://database/',
        ELASTICSEARCH_HOST: 'http://es:9200',
    },
    SYSTEM_EMAILS: {
        ADMIN: '',
        MAILER: '',
    },
};
