module.exports = {
    API_ADDRESS: 'https://api.finances-app.com:8092',
    INSTANCE: 'finances_prod',
    DATA: {
        MONGODB_HOST: 'mongodb://mongo/',
        ELASTICSEARCH_HOST: 'http://elasticsearch:9200',
    },
    SYSTEM_EMAILS: {
        ADMIN: 'admin@finances-app.com',
        MAILER: 'admin@finances-app.com',
    },
};
