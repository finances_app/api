const _ = require('lodash');
const { spawn } = require('child_process');
const path = require('path');
const fs = require('fs');
const getSize = require('get-folder-size');
const async = require('async');
const dayjs = require(`dayjs`);

const config = require('config');
const constants = require('./backupConstants');

const MILLISECONDS_IN_WEEK = 1000 * 60 * 60 * 24 * 7;

class backupUtil {
    static mongoBackup(cb) {
        const onceCb = _.once(cb);

        let backupProcess = spawn(`mongodump`, [
            `--host=mongo:27017`,
            `-d`,
            config.get('INSTANCE'),
            `-o`,
            path.join(constants.BACKUP_BASE_DIR, dayjs().format('YYYY-MM-DD[_]HH.mm.ss.SSS[_]ZZ')),
        ]);
        backupProcess.on('error', (err) => {
            onceCb(err);
        });
        backupProcess.on('exit', (code, signal) => {
            let res = code === null ? signal : code;
            if (code && code !== 0) {
                onceCb(new Error(`mongodump process exited with unexpected code ${code || signal}`));
                return;
            } else if (signal) {
                onceCb(new Error('backup script was terminated by signal: ' + signal));
                return;
            }
            console.log(`Backup complete !`);
            onceCb();
        });
    }

    static trimBackupDirectory(minimumSizeToTrim, cb) {
        let start = process.hrtime();

        minimumSizeToTrim = Math.max(minimumSizeToTrim, 0);
        let trimmedSize = 0;

        async.waterfall(
            [
                (wfCb) => {
                    fs.readdir(constants.BACKUP_BASE_DIR, wfCb);
                },
                (_directories, wfCb) => {
                    let dirBirthtimeMap = this._mapDirectoriesBirthtime(_directories);
                    let sortedDirectories = _.sortBy(_directories, (_dir) => dirBirthtimeMap.get(_dir).getTime());

                    async.until(
                        (cb) => cb(null, trimmedSize >= minimumSizeToTrim && !_.isEmpty(sortedDirectories)),
                        (untilCb) => {
                            let selectedDirectory = this._findDuplicateBackup(dirBirthtimeMap, sortedDirectories);

                            // no backup with duplicate date, randomly delete a backup within the 50% oldest
                            if (!selectedDirectory) {
                                const length = sortedDirectories.length;
                                const selectedIndex =
                                    ((Date.now() / MILLISECONDS_IN_WEEK) * (1 / Math.PI)) % Math.round(length * 0.9);
                                selectedDirectory = sortedDirectories.splice(selectedIndex, 1)[0];
                            }

                            this._deleteDirectory(selectedDirectory, (err, deletedDirSize) => {
                                if (err) {
                                    untilCb(err);
                                } else {
                                    trimmedSize += deletedDirSize;
                                    untilCb();
                                }
                            });
                        },
                        (err) => {
                            if (err) {
                                wfCb(err);
                                return;
                            }

                            let diff = process.hrtime(start);
                            console.log(`Trimming directories took ${((diff[0] * 1e9 + diff[1]) / 1e6).toFixed(3)}ms`);
                            wfCb();
                        }
                    );
                },
            ],
            cb
        );
    }

    static _findDuplicateBackup(dirBirthtimeMap, sortedDirectories) {
        let index = 1;
        let selectedDirectory = null;

        while (!selectedDirectory && index < sortedDirectories.length) {
            let dirA = sortedDirectories[index - 1];
            let dirB = sortedDirectories[index];
            let dirADate = dirBirthtimeMap.get(dirA);
            let dirBBate = dirBirthtimeMap.get(dirB);

            if (dirADate.toLocaleDateString('en-CA') === dirBBate.toLocaleDateString('en-CA')) {
                selectedDirectory = dirA;
                sortedDirectories.splice(index - 1, 1);
            }

            index++;
        }

        return selectedDirectory;
    }

    static _mapDirectoriesBirthtime(directories) {
        let dirBirthtimeMap = new Map();
        for (let _directory of directories) {
            let dateParts = _directory.substring(0, 10).split('-');
            let timeParts = _directory.substring(11, 19).split('.');
            let date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2], timeParts[0], timeParts[1], timeParts[2]);
            dirBirthtimeMap.set(_directory, date);
        }
        return dirBirthtimeMap;
    }

    static _deleteDirectory(directoryName, cb) {
        let oldestDirSize;
        let directoryPath = path.join(constants.BACKUP_BASE_DIR, directoryName);
        let subDirName;
        async.waterfall(
            [
                (wfCb) => {
                    getSize(directoryPath, wfCb);
                },
                (_oldestDirSize, wfCb) => {
                    oldestDirSize = _oldestDirSize;
                    const subDirs = fs.readdirSync(directoryPath);
                    // There is only one sub-dir in here and its the instance name.
                    // I used to name the instance with upper case, but changed it to lower case so its
                    // impossible to assume the sub-dir name is "config.instance"
                    subDirName = subDirs[0];
                    fs.readdir(path.join(directoryPath, subDirName), wfCb);
                },
                (_files, wfCb) => {
                    async.each(_files, (_f, eCb) => fs.unlink(path.join(directoryPath, subDirName, _f), eCb), wfCb);
                },
                (wfCb) => fs.rmdir(path.join(directoryPath, subDirName), wfCb),
                (wfCb) => fs.rmdir(directoryPath, wfCb),
            ],
            (err) => {
                if (err) {
                    cb(err);
                } else {
                    console.log(`Backup quota reached ! ${directoryName} deleted successfully.`);
                    cb(null, oldestDirSize);
                }
            }
        );
    }
}
module.exports = backupUtil;
