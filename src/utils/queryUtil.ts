import { isEmpty, extend } from 'lodash';
import { User, HydratedUser, IUserLocation, IUserDevice } from '../data/userCollection';
import { Request, Response, NextFunction, Router } from 'express';
import * as errorHandler from './errorHandler';
import { ClientError } from './errors';
import { verificationMiddleware } from '../_resources_/system/securityRoutes';
import { Context } from './context';

export interface AuthenticatedRequest extends Request {
    deviceId: string;
    userId: string;
    user: HydratedUser;
    context?: Context;
    hashedApiKey?: string;
}

export type LocationContext = {
    ip: string;
    device: IUserDevice;
    location: IUserLocation;
    locationLabel: string;
};

export interface PendingAuthenticationRequest extends AuthenticatedRequest {
    locationContext?: LocationContext;
    hashedApiKey?: string;
}

/**
 * Format response and add event data
 */
export async function prepareResponse(req: AuthenticatedRequest, response: any = {}) {
    const device = req.user.devices.find((_dev) => _dev._id.toString() === req.deviceId);
    let pendingCacheEvents = device.pendingCacheExpirationEvents || [];

    if (!isEmpty(pendingCacheEvents) && req.userId) {
        await User.updateOne(
            {
                googleUserId: req.userId,
                'devices._id': req.deviceId,
            },
            {
                $set: { 'devices.$.pendingCacheExpirationEvents': [] },
            }
        );
    }

    return extend(response, {
        pendingCacheEvents: pendingCacheEvents,
    });
}

export function makeMiddleware(handlerFn: (req: AuthenticatedRequest, res: Response) => Promise<void>) {
    return async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        try {
            await handlerFn(req, res);
            next();
        } catch (err) {
            if (err instanceof ClientError) {
                errorHandler.handle4XX(req, res, err.message, err.statusCode);
            } else {
                errorHandler.handle500(req, res, err);
            }
        }
    };
}

export function toInt(val: string) {
    return val ? parseInt(val) : null;
}

export function toFloat(val: string) {
    return val ? parseFloat(val) : null;
}

export function toBoolean(val: string) {
    let result = null;
    switch (val) {
        case 'true':
            result = true;
            break;
        case 'false':
            result = false;
            break;
    }
    return result;
}

export function toObject(val: string) {
    return (val && JSON.parse(decodeURIComponent(val))) || null;
}

export function makeRequestHandler<TResponse>(handlerFn: (req: AuthenticatedRequest, res: Response) => TResponse) {
    return async (req: AuthenticatedRequest, res: Response) => {
        try {
            let response = await handlerFn(req, res);
            res.send(await prepareResponse(req, response));
        } catch (err) {
            if (err instanceof ClientError) {
                errorHandler.handle4XX(req, res, err.message, err.statusCode);
            } else {
                errorHandler.handle500(req, res, err);
            }
        }
    };
}

export function makeAuthenticatedRouter() {
    const router = Router({});
    router.use(verificationMiddleware);
    return router;
}
