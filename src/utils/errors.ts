export class InternalError extends Error {
    constructor() {
        super(...arguments);
    }
}

export class ClientError extends Error {
    public statusCode: number;
    constructor(message: string, code = 400) {
        super(message);
        this.statusCode = code;
    }
}
