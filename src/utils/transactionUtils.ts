import * as _ from 'lodash';
import { EssentialTransactionData } from '../models/entities';
import { toMs } from 'ms-typescript';
const { removeDiacritics } = require('../import/importHelper');

export type TransactionDiff<
    TNew extends EssentialTransactionData,
    TExisting extends EssentialTransactionData = TNew
> = {
    added?: TNew[];
    removed?: TExisting[];
    equal?: TExisting[];
};

/**
 * Merge the local and remote transaction list into a diff collection.
 */
export function processDiff<TNew extends EssentialTransactionData, TExisting extends EssentialTransactionData>(
    newTransactions: TNew[],
    existingTransactions: TExisting[]
) {
    const diffs: TransactionDiff<TNew, TExisting>[] = [];

    iterateDiffs(newTransactions, existingTransactions, (added, removed) => {
        if (added && removed) {
            if (diffs.length && _.last(diffs).equal) {
                _.last(diffs).equal.push(removed);
            } else {
                diffs.push({ equal: [removed] });
            }
        } else if (added) {
            if (diffs.length && _.last(diffs).added) {
                _.last(diffs).added.push(added);
            } else {
                diffs.push({ added: [added] });
            }
        } else if (removed) {
            if (diffs.length && _.last(diffs).removed) {
                _.last(diffs).removed.push(removed);
            } else {
                diffs.push({ removed: [removed] });
            }
        }
    });

    return diffs;
}

/**
 * iterate over each difference or pair of matching items.
 */
export function iterateDiffs<TNew extends EssentialTransactionData, TExisting extends EssentialTransactionData>(
    newTransactions: TNew[],
    existingTransactions: TExisting[],
    predicate: (added: TNew, removed: TExisting) => void
) {
    // diffs should contain all transactions sorted by date and grouped by type of diff
    const local = prepareTransactions(newTransactions);
    const remote = prepareTransactions(existingTransactions);

    while (local.length || remote.length) {
        // mark removed if local is empty or if the first transaction of remote is not present in local
        const isRemoved =
            _.isEmpty(local) || (remote.length && local.every((_t) => !areTransactionSameIsh(_t, _.first(remote), 1)));
        // mark added if remote is empty or if the first transaction of local is not present in remote
        const isAdded =
            _.isEmpty(remote) || (local.length && remote.every((_t) => !areTransactionSameIsh(_t, _.first(local), 1)));

        if (isRemoved) {
            predicate(null, remote.shift());
        } else if (isAdded) {
            predicate(local.shift(), null);
        } else {
            if (_.first(remote).date.getTime() <= _.first(local).date.getTime()) {
                let transaction = remote.shift();
                let spliced = local.splice(
                    local.findIndex((_t) => areTransactionSameIsh(_t, transaction, 1)),
                    1
                );
                predicate(spliced[0], transaction);
            } else {
                let transaction = local.shift();
                let spliced = remote.splice(
                    remote.findIndex((_t) => areTransactionSameIsh(_t, transaction, 1)),
                    1
                );
                predicate(transaction, spliced[0]);
            }
        }
    }
}

/**
 * Convert dates to Date objects and sort by date.
 */
function prepareTransactions<T extends EssentialTransactionData>(transactions: T[]) {
    return _.chain(transactions)
        .map((_t) => {
            _t.date = new Date(_t.date);
            return _t;
        })
        .sortBy((_t) => removeDiacritics(_t.description))
        .sortBy((_t) => _t.date.getTime())
        .value();
}

/**
 * Compare transactions, allowing N minor property to be different
 */
export function areTransactionSameIsh<T1 extends EssentialTransactionData, T2 extends EssentialTransactionData>(
    a: T1,
    b: T2,
    allowedDifferences = 0
) {
    return compareTransactions(a, b) <= allowedDifferences;
}

/**
 * Compare transactions by date, amount, type and account.
 * Will return 999 if one of the items is null or if they are not comparable.
 */
function compareTransactions<T1 extends EssentialTransactionData, T2 extends EssentialTransactionData>(a: T1, b: T2) {
    if (!a || !b) {
        return 999;
    } else if (Math.abs(a.date.getTime() - b.date.getTime()) > toMs('2h')) {
        return 999;
    } else if (a.amount !== b.amount) {
        return 999;
    }

    let count = 0;
    if (prepareDescription(a.description || '') !== prepareDescription(b.description || '')) {
        count++;
    }
    if (a.account !== b.account) {
        count++;
    }
    return count;
}

export function findUniqAccounts<T extends EssentialTransactionData>(transactions: T[]) {
    return _.chain(transactions)
        .map((_t) => _t.account)
        .uniq()
        .filter(Boolean)
        .value();
}

export function prepareDescription(description: string) {
    return description.trim().replace(/\s+/gi, ' ').toUpperCase();
}
