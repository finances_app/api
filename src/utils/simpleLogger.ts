import * as _cluster from 'cluster';
import { captureException, captureMessage } from '@sentry/node';
const cluster = (_cluster as unknown) as _cluster.Cluster; // typings fix

const dateFormat = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    timeZoneName: 'short',
};

export function log(message: string) {
    console.log(prependSystemInfo(message));
    captureMessage(message, 'log');
}

export function warning(message: string) {
    console.warn(prependSystemInfo(message));
    captureMessage(message, 'warning');
}

export function error(error: Error | string) {
    console.error(prependSystemInfo(error));
    if (error instanceof Error) {
        captureException(error);
    } else {
        captureMessage(error, 'error');
    }
}

function prependSystemInfo(message: any) {
    let workerName;
    if (cluster.isPrimary) {
        workerName = '[master]';
    } else {
        let paddedId = `#${cluster.worker.id}`;
        workerName = cluster.isPrimary ? '[master]' : `[worker ${paddedId.padStart(3, ' ')}]`;
    }
    // @ts-ignore
    let timestamp = new Date().toLocaleString('fr-CA', dateFormat);
    if (message instanceof Error) {
        message = JSON.stringify({ name: message.name, message: message.message, stack: message.stack }, null, 2);
    }
    return workerName + ' ' + timestamp + ' ' + message;
}
