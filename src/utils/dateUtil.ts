/**
 * Created by hugo on 2018-05-18
 */

import * as dayjs from 'dayjs';
import { Dayjs } from 'dayjs';
import * as weekOfYear from 'dayjs/plugin/weekOfYear';
dayjs.extend(weekOfYear);

export type SimpleDateInterval = 'w' | 'M' | 't' | 'y';
export type DateInterval = SimpleDateInterval | '2w' | 'q';

export function formatDateTime(date: string, time: string, options: { separator: string; format: string }) {
    if (options === undefined && typeof time === 'object') {
        options = time;
        time = undefined;
    }

    let separator = options.separator;
    if (!separator) {
        if (options.format.includes('-')) separator = '-';
        else if (options.format.includes('/')) separator = '/';
    }

    let splitDate = date.split(separator);
    let timeSplit;
    if (time !== undefined) {
        timeSplit = time.split(':');
    }

    switch (options.format) {
        case `dd${separator}MM${separator}yyyy`: {
            return new Date(
                parseInt(splitDate[2]),
                parseInt(splitDate[1]) - 1,
                parseInt(splitDate[0]),
                timeSplit ? parseInt(timeSplit[0]) : null,
                timeSplit ? parseInt(timeSplit[1]) : null,
                timeSplit ? parseInt(timeSplit[2]) : null
            );
        }
        case `MM${separator}dd${separator}yyyy`: {
            return new Date(
                parseInt(splitDate[2]),
                parseInt(splitDate[0]) - 1,
                parseInt(splitDate[1]),
                timeSplit ? parseInt(timeSplit[0]) : null,
                timeSplit ? parseInt(timeSplit[1]) : null,
                timeSplit ? parseInt(timeSplit[2]) : null
            );
        }
        case `yyyy${separator}MM${separator}dd`: {
            return new Date(
                parseInt(splitDate[0]),
                parseInt(splitDate[1]) - 1,
                parseInt(splitDate[2]),
                timeSplit ? parseInt(timeSplit[0]) : null,
                timeSplit ? parseInt(timeSplit[1]) : null,
                timeSplit ? parseInt(timeSplit[2]) : null
            );
        }
    }

    return null;
}

export function startOfPeriod(period: DateInterval, fromDate = dayjs()) {
    if (period === 't') {
        let month = dayjs(fromDate).get('M');
        return dayjs(fromDate)
            .set('M', Math.floor(month / 4) * 4)
            .startOf('M');
    } else if (period === 'q') {
        let month = dayjs(fromDate).get('M');
        return dayjs(fromDate)
            .set('M', Math.floor(month / 3) * 3)
            .startOf('M');
    } else if (period === '2w') {
        let week = dayjs(fromDate).week();
        return dayjs(fromDate)
            .week(Math.floor(week / 2) * 2)
            .startOf('w');
    }
    return dayjs(fromDate).startOf(period);
}

export function endOfPeriod(period: DateInterval, fromDate = dayjs()) {
    if (period === 't') {
        let month = dayjs(fromDate).get('M');
        return dayjs(fromDate)
            .set('M', Math.floor(month / 4) * 4)
            .add(3, 'M')
            .endOf('M');
    } else if (period === 'q') {
        let month = dayjs(fromDate).get('M');
        return dayjs(fromDate)
            .set('M', Math.floor(month / 3) * 3)
            .add(2, 'M')
            .endOf('M');
    } else if (period === '2w') {
        let week = dayjs(fromDate).week();
        return dayjs(fromDate)
            .week(Math.floor(week / 2) * 2)
            .add(1, 'w')
            .endOf('w');
    }
    return dayjs(fromDate).endOf(period);
}

export function startOfYear(year: number) {
    return dayjs().year(year).startOf('year');
}

export function endOfYear(year: number) {
    return dayjs().year(year).endOf('year');
}

export function splitDateRangeByYears(from: Dayjs, to: Dayjs) {
    let dateRanges = [];

    const yearDifference = to.year() - from.year();
    const startYear = from.year();

    for (let diff = 0; diff <= yearDifference; diff++) {
        let yearFrom = dayjs()
            .year(startYear + diff)
            .startOf('year');
        let yearTo = dayjs()
            .year(startYear + diff)
            .endOf('year');

        if (diff === 0) {
            yearFrom = from;
        }
        if (diff === yearDifference) {
            yearTo = to;
        }

        dateRanges.push({ from: yearFrom, to: yearTo });
    }

    return dateRanges;
}

/**
 * Clone the `date` and move it forward by one `period` and return it.
 */
export function addOnePeriod(date: Dayjs, period: DateInterval) {
    if (period === '2w') {
        date = date.add(2, 'week');
    } else if (period === 't') {
        date = date.add(4, 'month');
    } else if (period === 'q') {
        date = date.add(3, 'month');
    } else {
        date = date.add(1, period);
    }
    return date;
}

export function periodsInYear(period: DateInterval) {
    switch (period) {
        case 'w':
            return 52;
        case '2w':
            return 26;
        case 'M':
            return 12;
        case 'q':
            return 4;
        case 't':
            return 3;
        case 'y':
            return 1;
        default:
            return null;
    }
}

export function daysInYear(year: number) {
    return year % 4 === 0 ? 366 : 365;
}
