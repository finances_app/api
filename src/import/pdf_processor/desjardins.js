const importHelper = require('../importHelper');
const pdfHelper = require('./searchablePdfHelper');
const _ = require('lodash');
const { AccountType, parseAccountType, isLoanAccount, isCreditCard } = require('../../models/enums');

/**
 * Represent a record from a Desjardins PDF statement.
 */
class DesjardinsPdfStatementRecord {
    constructor({ account, date, description, interest, fee, amount, balance }) {
        this.account = account;
        this.date = date;
        this.description = description;
        this.interest = interest;
        this.fee = fee;
        this.amount = amount;
        this.balance = balance;

        Object.seal(this);
    }
}

class desjardinsPdfProcessor {
    /**
     * Process a Desjardins PDF statement into transactions.
     * @param textPages {string[]}
     * @returns {Transaction[]}
     */
    static process(textPages) {
        let records = this._getRecords(textPages);
        return records.map((r) => ({
            description: r.description,
            date: r.date,
            amount: r.amount,
            account: r.account,
            currency: 'CAD',
        }));
    }

    /**
     * Extract the most recent balance for each account in the statement
     * @param textPages
     */
    static getBalance(textPages) {
        // there is no easy way to get the balance of each account for Desjardins statement, so we simply extract them from the records.

        let records = this._getRecords(textPages);

        let accounts = [];

        for (let [accountId, _records] of Object.entries(_.groupBy(records, (_r) => _r.account))) {
            let newestRecord = _.chain(_records)
                .sortBy((_r) => _r.date.getTime())
                .last()
                .value();
            accounts.push({
                accountName: accountId,
                date: newestRecord.date,
                balance: newestRecord.balance,
            });
        }

        return accounts;
    }

    static _getRecords(textPages) {
        let firstPageLines = pdfHelper.splitIntoLines(textPages[0]);
        let statementDate = this._findStatementDate(firstPageLines);

        let records = [];

        let rawLines = textPages.reduce((acc, page) => acc.concat(pdfHelper.splitIntoLines(page)), []);
        let processableLineGroups = this._findProcessableDesjardinsLinesByAccount(rawLines);
        for (let key in processableLineGroups) {
            if (!processableLineGroups.hasOwnProperty(key)) continue;

            let lineBalance = 0;
            let processableLines = processableLineGroups[key];
            let recordsInGroup = this._groupProcessableDesjardinsLines(processableLines)
                .filter((group) => {
                    if (pdfHelper.findWords(group[0]).join(' ').startsWith('Solde reporté')) {
                        let amounts = pdfHelper.findAmountsWithinLine(group[0]);
                        lineBalance = amounts[0];
                        return false;
                    }
                    return true;
                })
                .map((group) => {
                    let record = this._parseDesjardinsLineGroup(group, lineBalance, key, statementDate);
                    lineBalance = record.balance;
                    return record;
                });
            records = records.concat(recordsInGroup);
        }

        return records;
    }

    /**
     * Get statement date from the first page
     * @param rawLines {String[]}
     * @returns {Date}
     * @private
     */
    static _findStatementDate(rawLines) {
        let words = pdfHelper.findWords(rawLines[5]);
        if (words.length === 7 && words.includes('du') && words.includes('au')) {
            return this._parseDesjardinsDateLine(words);
        } else {
            throw new Error('Error extracting date from Desjardins statement. Date expected at line 6.');
        }
    }

    /**
     * Find processable lines from a Desjardins statement page. Returns a dictionary where each
     * key is the account type and the value an array of lines.
     * @param rawLines {String[]}
     * @returns {Object.<String, String[]>}
     * @private
     */
    static _findProcessableDesjardinsLinesByAccount(rawLines) {
        const ACCOUNT_HEADER = 'Date Code Description Frais Retrait Dépôt Solde';
        const CREDIT_MARGIN_HEADER = 'Date Code Description Intérêt Avance Remboursement Solde';
        const LOAN_HEADER = 'Date Code Description Intérêt Capital versé Remboursement Solde';
        const SAVINGS_HEADER = 'Date Code Description Retrait Dépôt Solde';
        const KNOWN_HEADERS = [ACCOUNT_HEADER, CREDIT_MARGIN_HEADER, LOAN_HEADER, SAVINGS_HEADER];

        let processableLinesGroups = {};

        let folio = this._findFolioNumber(rawLines);
        if (!folio) {
            throw new Error('Could not find the folio number in the header.');
        }

        let currentAccount = null;
        let currentLines = [];
        for (let i = 0; i < rawLines.length; i++) {
            let line = rawLines[i];

            let words = pdfHelper.findWords(line);
            let joinedWords = words.join(' ');

            if (KNOWN_HEADERS.some((HEADER) => joinedWords.startsWith(HEADER))) {
                if (pdfHelper.isLineAccountTypeHeader(rawLines[i - 1])) {
                    let wordGroups = pdfHelper.findWordGroups(rawLines[i - 1], 3);
                    currentAccount = this._formatAccountName(pdfHelper.findWords(wordGroups[0]).join(''), folio);
                } else if (pdfHelper.isLineAccountTypeHeader(rawLines[i + 2])) {
                    let wordGroups = pdfHelper.findWordGroups(rawLines[i + 2], 3);
                    currentAccount = this._formatAccountName(pdfHelper.findWords(wordGroups[0]).join(''), folio);
                    i += 2; // skip two lines, records start 3 lines after header.
                } else {
                    // throw new Error(
                    //     'No account type line found here expected (one line before or two lines after header).'
                    // );
                }
            } else if (currentAccount !== null) {
                const isEOF = line === '\f';
                const isEmptyLine = line === '';
                const isEndOfMCSection =
                    currentAccount.includes(AccountType.MC) && joinedWords.startsWith('Intérêts à jour');
                const isEndOfPRSection =
                    currentAccount.includes(AccountType.PR) &&
                    (joinedWords.startsWith("Date d'échéance prévue") || joinedWords.startsWith('SOMMAIRE ANNUEL'));

                if (isEOF || isEmptyLine || isEndOfMCSection || isEndOfPRSection) {
                    if (processableLinesGroups[currentAccount])
                        processableLinesGroups[currentAccount] = processableLinesGroups[currentAccount].concat(
                            currentLines
                        );
                    else processableLinesGroups[currentAccount] = currentLines;
                    currentAccount = null;
                    currentLines = [];
                } else if (pdfHelper.isLineAccountTypeHeader(line)) {
                    // end of records for other account type
                    if (processableLinesGroups[currentAccount])
                        processableLinesGroups[currentAccount] = processableLinesGroups[currentAccount].concat(
                            currentLines
                        );
                    else processableLinesGroups[currentAccount] = currentLines;
                    let wordGroups = pdfHelper.findWordGroups(line, 3);
                    currentAccount = this._formatAccountName(pdfHelper.findWords(wordGroups[0]).join(''), folio);
                    currentLines = [];
                } else {
                    // should be a record line
                    currentLines.push(line);
                }
            }
        }

        return processableLinesGroups;
    }

    static _findFolioNumber(rawLines) {
        const folioLineIndex = rawLines.findIndex((_line) => {
            let words = pdfHelper.findWords(_line);
            return words.some((_word) => _word === 'Folio');
        });

        if (folioLineIndex !== -1) {
            const folioWords = pdfHelper.findWords(rawLines[folioLineIndex + 1]);
            return folioWords.find((_word) => /^\d{6}$/gi.test(_word));
        }
    }

    static _formatAccountName(name, folio) {
        return `${folio}-${name}`;
    }

    /**
     * Group processable lines from a Desjardins statement into individual statement records.
     * @param processableLines {String[]}
     * @returns {String[][]}
     * @private
     */
    static _groupProcessableDesjardinsLines(processableLines) {
        let lineGroups = [];

        let currentGroup = [];
        for (let line of processableLines) {
            if (pdfHelper.isFirstLineOfGroup(line)) {
                if (currentGroup.length) {
                    lineGroups.push(currentGroup);
                    currentGroup = [line];
                } else currentGroup.push(line);
            } else {
                currentGroup.push(line);
            }
        }
        if (currentGroup.length) lineGroups.push(currentGroup);

        return lineGroups;
    }

    /**
     * Parse a group of line representing a single Desjardins statement record into an array of columns.
     * @param lines {String[]}
     * @param previousLineBalance {Number}
     * @param accountName {String}
     * @param statementDate {Date}
     * @returns {DesjardinsPdfStatementRecord}
     * @private
     */
    static _parseDesjardinsLineGroup(lines, previousLineBalance, accountName, statementDate) {
        // one line:    expected order [day] [month] [code] [description] [amount] [balance]
        // two lines:   ether two lines description OR code overflow into second line
        //              first line: [day] [month] [code] [description] second line: [description] [amount] [balance]
        //              OR first line: [day] [month] [description] second line: [code] [amount] [balance]
        // tree lines:  two lines description AND code overflow into second line
        //              first line: [day] [month] [description] second line: [code] third line: [description] [amount] [balance]

        let firstLineWords = pdfHelper.findWords(lines[0]);

        let amounts = this._findDesjardinsAmounts(lines, previousLineBalance, accountName);
        let description = this._findDesjardinsDescription(lines);

        const split = importHelper.splitDesjardinsDescription(description);

        return new DesjardinsPdfStatementRecord({
            account: accountName,
            date: new Date(
                statementDate.getFullYear(),
                importHelper.shortMonthStringToInt(firstLineWords[1]),
                parseInt(firstLineWords[0])
            ),
            description: split[0] ? split.join(' /') : split[1],
            balance: amounts.balance,
            fee: amounts.fee,
            interest: amounts.interest,
            amount: amounts.amount,
        });
    }

    /**
     * Parse lines to find the amounts and balance from the Desjaridns transaction.
     * @param lines {String[]}
     * @param previousLineBalance {Number}
     * @param accountName {String}
     * @returns {{interest: Number, fee: Number, amount: Number, balance: Number}}
     * @private
     */
    static _findDesjardinsAmounts(lines, previousLineBalance, accountName) {
        const accountType = parseAccountType(accountName);

        let amounts = [];
        for (let line of lines) {
            amounts = amounts.concat(pdfHelper.findAmountsWithinLine(line));
        }

        let interest;
        let fee;
        if (amounts.length === 3) {
            if (isLoanAccount(accountType)) {
                interest = amounts[0];
            } else {
                fee = amounts[0];
            }
        }

        let balance = amounts.pop();
        let amount = Math.abs(amounts.pop() || 0);
        let balanceDifference = previousLineBalance - balance;

        // Bug fix note: There is an edge case with credit line type accounts where the first transaction is an interest
        // widthdrawal, the reported balance will be the same before and after the transaction.
        const isLineCreditEdgeCase = accountType === AccountType.MC && balanceDifference === 0;
        if (Math.abs(Math.abs(amount) - Math.abs(balanceDifference)) > 0.01 && !isLineCreditEdgeCase) {
            throw new Error(`Unexpected balance change from last record. From ${previousLineBalance} to ${balance} with 
            amount ${amount} (difference: ${balanceDifference})`);
        }

        const isLoanOrCreditCard = isLoanAccount(accountType) || isCreditCard(accountType);
        if ((isLoanOrCreditCard && balanceDifference < 0) || (!isLoanOrCreditCard && balanceDifference > 0)) {
            amount *= -1;
        }

        return { interest, fee, amount, balance };
    }

    /**
     * Extract the description from the transaction lines. Handle descriptions on two lines AND transaction code overflow
     * into second line (pdf parsing error).
     * @param lines {String[]}
     * @returns {String}
     * @private
     */
    static _findDesjardinsDescription(lines) {
        let description = '';
        let firstLineWords = pdfHelper.findWords(lines[0]);

        switch (lines.length) {
            // case 1: {
            //
            // }
            case 2: {
                let secondLineWordGroups = pdfHelper.findWordGroups(lines[1], 5);
                // To increase confidence, check the two places where transaction code is expected.
                // .. Chances are very low that first word of description AND first word of second line match the code regexp.
                if (
                    this._isDesjardinsTransactionCode(firstLineWords[2]) &&
                    !this._isDesjardinsTransactionCode(secondLineWordGroups[0])
                ) {
                    description = pdfHelper.removeWords(lines[0], 3);
                    description = pdfHelper.findWordGroups(description, 5)[0] + ' ' + secondLineWordGroups[0];
                } else {
                    description = pdfHelper.removeWords(lines[0], 2);
                }
                return description;
            }
            case 3: {
                description = pdfHelper.removeWords(lines[0], 2);
                return description + ' ' + pdfHelper.findWordGroups(lines[2], 5)[0];
            }
            default:
                // if more than 3 lines are captured, just assume anything past the first one is garbage.
                description = pdfHelper.removeWords(lines[0], 2).trim().substr(firstLineWords[2].length);

                return pdfHelper.findWordGroups(description, 5)[0];
            // throw new Error('Unexpected number of lines within Desjardins line group.');
        }
    }

    /**
     * Test if a word has the format of a transaction code (2 or 3 upper case chars).
     * @param word {String}
     * @returns {boolean}
     * @private
     */
    static _isDesjardinsTransactionCode(word) {
        let transactionCodeREgex = /^[A-Z][A-Z][A-Z]?$/;
        return transactionCodeREgex.test(word);
    }

    /**
     * Parse the line in a Desjardins statement containing the date.
     * @param dateLineWords {String[]}
     * @returns {Date}
     * @private
     */
    static _parseDesjardinsDateLine(dateLineWords) {
        let day = parseInt(dateLineWords[1]);
        let month;
        let year = parseInt(dateLineWords[6]);

        month = importHelper.fullMonthStringToInt(dateLineWords[2]);
        if (month === -1) throw new Error(`Error converting month string to integer. (value: ${dateLineWords[2]})`);

        return new Date(year, month, day);
    }
}

module.exports = desjardinsPdfProcessor;
