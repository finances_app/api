const _ = require('lodash');
const importHelper = require('../importHelper');
const pdfHelper = require('./searchablePdfHelper');

/**
 * Represent a record from a Visa PDF statement.
 */
class VisaPdfStatementRecord {
    constructor({ account, transactionDate, inscriptionDate, transactionNumber, description, amount }) {
        this.account = account;
        this.transactionDate = transactionDate;
        this.inscriptionDate = inscriptionDate;
        this.transactionNumber = transactionNumber;
        this.description = description;
        this.amount = amount;

        Object.seal(this);
    }
}

class visaPdfProcessor {
    /**
     * Process a Visa PDF Statement into transactions.
     * @param textPages {string[]}
     * @returns {Transaction[]}
     */
    static process(textPages) {
        let firstPageLines = pdfHelper.splitIntoLines(textPages[0]);
        let statementDate = this._findStatementDate(firstPageLines);
        const isNewFormat = statementDate.getTime() >= new Date(2020, 10, 1).getTime();

        let records = [];

        let rawLines = textPages.reduce((acc, page) => acc.concat(pdfHelper.splitIntoLines(page)), []);
        _.forEach(this._findProcessableVisaLines(rawLines), (processableLines, cardNumber) =>
            processableLines
                .map((line) => this._normalizeVisaLine(line, isNewFormat))
                .forEach((line) => {
                    let recordMonth = parseInt(line[1]) - 1;
                    let recordYear = statementDate.getFullYear();
                    if (recordMonth > statementDate.getMonth()) recordYear--;
                    records.push(
                        new VisaPdfStatementRecord({
                            account: `${cardNumber.split(' ').pop()}-CC`,
                            transactionDate: new Date(recordYear, recordMonth, parseInt(line[0])),
                            inscriptionDate: new Date(recordYear, recordMonth, parseInt(line[2])),
                            transactionNumber: parseInt(line[4]),
                            description: line[5],
                            amount: importHelper.parseVisaAmount(line[6]),
                        })
                    );
                })
        );

        return records.map((r) => ({
            description: r.description,
            date: r.transactionDate,
            amount: r.amount,
            account: r.account,
            currency: 'CAD',
        }));
    }

    /**
     * Extract the most recent balance for each account in the statement
     * @param textPages
     * @returns {{ accountName: string, date: Date, balance: number }[]}
     */
    static getBalance(textPages) {
        let rawLines = textPages.reduce((acc, page) => acc.concat(pdfHelper.splitIntoLines(page)), []);
        const cardNumber = this._findCardNumber(rawLines);
        return [
            {
                accountName: `${cardNumber.split(' ').pop()}-CC`,
                date: this._findStatementDate(rawLines),
                balance: this._findStatementBalance(rawLines),
            },
        ];
    }

    static _findCardNumber(rawLines) {
        // using findLast because we want to get the latest card number (if there is more than one present)
        const cardNumberLine = _.findLast(rawLines, (_line) =>
            _line.trim().startsWith('Transactions effectuées avec la carte de')
        );
        return pdfHelper.findWords(cardNumberLine).splice(-4).join(' ');
    }

    /**
     * Get statement date from the first page
     * @param rawLines {String[]}
     * @returns {Date}
     * @private
     */
    static _findStatementDate(rawLines) {
        let dateLine = null;
        rawLines.forEach((line, index) => {
            if (dateLine) return;

            let words = pdfHelper.findWords(line);
            if (words.join(' ').startsWith('Date du Jour Mois Année')) {
                dateLine = rawLines[index + 3];
            }
        });
        return this._parseVisaDateLine(dateLine);
    }

    static _findStatementBalance(rawLines) {
        let balanceLine = null;
        rawLines.forEach((line, index) => {
            if (balanceLine) return;

            let words = pdfHelper.findWords(line);
            if (words.join(' ').endsWith('Nouveau solde courant')) {
                balanceLine = rawLines[index + 4];
            }
        });
        return importHelper.parseVisaAmount(pdfHelper.findWordGroups(balanceLine, 2).pop());
    }

    /**
     * Find all lines containing a record within a page.
     * @param rawLines {String[]}
     * @returns {Object.<string, String[]>} processable lines grouped by card numbers
     * @private
     */
    static _findProcessableVisaLines(rawLines) {
        let processableLineGroups = {};
        let processableLines = [];

        let headerFound = false;
        let nonProcessableLineCount = 0;
        let lastEncounteredCardNumber = '';
        for (let line of rawLines) {
            let words = pdfHelper.findWords(line);
            const wordGroups = pdfHelper.findWordGroups(line, 2);
            const isHeader = words.join(' ').startsWith('Transactions effectuées avec la carte de');
            if (isHeader) {
                lastEncounteredCardNumber = words.slice(-4).join(' ');
            }
            let isLastHeaderLine = words.join(' ').startsWith('J M J M');

            if (isLastHeaderLine) {
                headerFound = true;
            } else if (headerFound) {
                if (words.join(' ').startsWith('PROGRAMME DE RÉCOMPENSES') || nonProcessableLineCount === 2) {
                    headerFound = false;
                    nonProcessableLineCount = 0;
                } else if (
                    processableLines.length === 0 ||
                    pdfHelper.isWordPositionMatching(processableLines[0], line, 5)
                ) {
                    processableLines.push(line);
                    nonProcessableLineCount = 0; // This is really fucked up code, here is the explanation:
                    // Transactions in other currency have a 2nd line detailing the conversion rate.
                    // This 2nd line will increment the nonProcessableLineCount, but its not really what this variable is for.
                    // To not break this wierd logic I dont completely understand, we reset this counter to 0 as if nothing happened.
                } else if (
                    (wordGroups[0]?.toLowerCase() === 'total' && wordGroups.length === 2) ||
                    wordGroups[wordGroups.length - 2]?.toLowerCase() === 'total'
                ) {
                    this._checkSectionTransactionIntegrity(processableLines, line);
                    // add transaction to groups, reset the processable lines array
                    if (processableLineGroups[lastEncounteredCardNumber]) {
                        processableLineGroups[lastEncounteredCardNumber].push(...processableLines);
                    } else {
                        processableLineGroups[lastEncounteredCardNumber] = processableLines;
                    }
                    processableLines = [];
                    headerFound = false;
                } else {
                    nonProcessableLineCount++;
                }
            }
        }

        // remaining section is always the account operations section (therefore no total line for this type and we need
        // to append them manually)
        if (processableLineGroups[lastEncounteredCardNumber]) {
            processableLineGroups[lastEncounteredCardNumber].push(...processableLines);
        } else {
            processableLineGroups[lastEncounteredCardNumber] = processableLines;
        }

        return processableLineGroups;
    }

    static _checkSectionTransactionIntegrity(processableLines, totalLine) {
        let words = pdfHelper.findWordGroups(totalLine, 2);
        let sectionTotal = importHelper.parseVisaAmount(_.last(words));

        let processableLinesTotal = processableLines.reduce((acc, line) => {
            const words = pdfHelper.findWords(line);
            return acc + importHelper.parseVisaAmount(this._extractAmountFromLine(words));
        }, 0);
        if (Math.abs(sectionTotal - processableLinesTotal) > 0.01)
            throw new Error(
                `Error finding processable lines, total does not match sum of processable lines. (Total: ${sectionTotal}, sum: ${processableLinesTotal})`
            );
    }

    /**
     * Parse processable a line from Visa statement into array of data columns.
     * @param processableLine {String}
     * @param isNewFormat {boolean}
     * @returns {Array}
     * @private
     */
    static _normalizeVisaLine(processableLine, isNewFormat) {
        let normalizedLine = [];
        let words = pdfHelper.findWords(processableLine);
        const wordsToRemove = isNewFormat ? 4 : 5;
        for (let i = 0; i < wordsToRemove; i++) {
            normalizedLine.push(words.shift());
        }
        if (isNewFormat) {
            normalizedLine.push('');
        }
        let amountWord = this._extractAmountFromLine(words);
        if (isNewFormat && _.last(words) === '%') {
            words.pop();
            words.pop();
        }

        // the little description processing to preserve whitespace padding !
        const escapedDescription = words
            .join(' ')
            .replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')
            .replace(/\\ /g, '\\s+');
        const results = new RegExp(escapedDescription, 'g').exec(processableLine);
        const location = pdfHelper.findWords(results[0].substring(25)).join(' ').padEnd(16, ' ');
        const description = `${results[0].substring(0, 25)}${location}`;

        normalizedLine.push(description.trim());
        normalizedLine.push(amountWord);
        return normalizedLine;
    }

    static _extractAmountFromLine(words) {
        let amountWord = words.pop();
        // handle corner case where the amount is over 1000 $
        if (_.last(words) !== '%' && parseInt(_.last(words))) {
            amountWord = `${words.pop()} ${amountWord}`;
        }
        return amountWord;
    }

    /**
     * Parse the line in a Visa statement containing the date.
     * @param dateLine {String}
     * @returns {Date}
     * @private
     */
    static _parseVisaDateLine(dateLine) {
        let dateLineWords = pdfHelper.findWords(dateLine);
        let day = parseInt(dateLineWords[0]);
        let month = parseInt(dateLineWords[1]);
        let year = parseInt(dateLineWords[2]);
        return new Date(year, month - 1, day);
    }
}

module.exports = visaPdfProcessor;
