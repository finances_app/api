/**
 * Created by hugo on 2018-05-18.
 */
const _ = require('lodash');

const dateUtil = require('../../utils/dateUtil');
const importConfig = require('config').get('IMPORT.desjardins');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Public functions
///
module.exports = {
    process: function (lines) {
        validate(lines);

        let records = getRecords(lines);
        return records
            .filter((r) => r.amount !== 0)
            .map((r) => ({
                description: r.description,
                date: r.date,
                amount: r.amount,
                account: `${r.accountNumber}-${r.accountName}`,
                currency: 'CAD',
            }));
    },

    /**
     * @param {string[]} lines
     * @returns {{ accountName: string, date: Date, balance: number }[]}
     */
    getBalance: function (lines) {
        validate(lines);

        let records = getRecords(lines);
        let accounts = [];

        for (let [accountId, _records] of Object.entries(_.groupBy(records, (_r) => _r.accountName))) {
            let newestRecord = _.chain(_records)
                .sortBy((_r) => _r.date.getTime() * -1)
                .first()
                .value();
            accounts.push({
                // TODO: change this to use the accountNumber + accountName some day
                accountName: accountId,
                date: newestRecord.date,
                balance: newestRecord.balance,
            });
        }
        return accounts;
    },
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Private classes
///
class DesjardinsRecord {
    constructor({ branchName, accountNumber, accountName, date, transactionNumber, description, amount, balance }) {
        this.branchName = branchName;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.date = date;
        this.transactionNumber = transactionNumber;
        this.description = description;
        this.amount = amount;
        this.balance = balance;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Private functions
///
function validate(lines) {
    if (lines.length < importConfig.minimum_line_count) {
        throw new Error(`Unexpected number of lines. Expected ${importConfig.minimum_line_count}, got ${lines.length}`);
    }

    let line;
    for (let i = 1; i < lines.length - 1; i++) {
        line = lines[i];

        if (line.length !== importConfig.column_count) {
            throw new Error(`Error at line ${i}. Expected ${importConfig.column_count} columns, got ${line.length}.`);
        }

        for (let key in importConfig.columns_type) {
            if (typeof line[key] !== importConfig.columns_type[key]) {
                throw new Error(
                    `Error at line ${i}. Invalid data type at column ${key}. Expected ${
                        importConfig.columns_type[key]
                    }, got ${typeof line[key]}.`
                );
            }
        }

        let dateSplit = line[3].split('/');
        if (dateSplit.length !== 3) {
            throw new Error(`Error at line ${i}. Unexpected date format.`);
        }
    }
}

/**
 * @param {string[]} lines
 * @returns {DesjardinsRecord[]}
 */
function getRecords(lines) {
    return lines
        .filter((l) => l.length === 14 && !l[2].startsWith('PR'))
        .map((l) => {
            // If record is in a line of credit account..
            if (l[2].startsWith('MC')) {
                return new DesjardinsRecord({
                    branchName: l[0],
                    accountNumber: l[1].toString(),
                    accountName: l[2],
                    date: dateUtil.formatDateTime(l[3].toString().replace('-', '/'), {
                        format: 'yyyy/MM/dd',
                    }),
                    transactionNumber: parseInt(l[4]),
                    description: l[5],
                    amount: parseAmount('-' + l[11]) || parseAmount(l[12]) || 0,
                    balance: parseFloat(l[13]),
                });
            } else {
                return new DesjardinsRecord({
                    branchName: l[0],
                    accountNumber: l[1].toString(),
                    accountName: l[2],
                    date: dateUtil.formatDateTime(l[3].toString().replace('-', '/'), {
                        format: 'yyyy/MM/dd',
                    }),
                    transactionNumber: parseInt(l[4]),
                    description: l[5],
                    amount: parseAmount('-' + l[7]) || parseAmount(l[8]) || 0,
                    balance: parseFloat(l[13]),
                });
            }
        });
}

function parseAmount(amount) {
    if (!amount || amount === '-') return null;
    return parseFloat(amount.toString().replace(',', '.'));
}
