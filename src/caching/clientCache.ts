import { HydratedUser } from '../data/userCollection';
import { Category } from '../data/categoryCollection';
import { EssentialTransactionData } from '../models/entities';

export async function createCacheExpirationEvents(user: HydratedUser, transactions: EssentialTransactionData[]) {
    let cacheExpirationEvents = [];

    let excludedCategoryNames = await Category.getTransferCategoryNames(user.googleUserId);

    if (transactions.filter((_t) => !excludedCategoryNames.includes(_t.category)).length > 0) {
        cacheExpirationEvents.push('NON_TRANSFER_TRANSACTIONS_IMPORTED');
    } else if (transactions.length > 0) {
        cacheExpirationEvents.push('TRANSACTIONS_IMPORTED');
    }

    await user.addCacheExpirationEvents(cacheExpirationEvents);
}
