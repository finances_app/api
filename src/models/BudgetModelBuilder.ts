import * as dayjs from 'dayjs';
import * as _ from 'lodash';
import { Category, LeanCategory } from '../data/categoryCollection';
import NTree, { TreeNode } from './nTree';
import { CategoryType, SystemCategory } from './enums';
import { getCategories } from '../_resources_/budget/budgetController';
import { Budget, BudgetDocument } from '../data/budgetCollection';
import { searchTransactions } from '../_resources_/transaction/transactionController';
import { LeanTransaction, Transaction } from '../data/transactionCollection';
import { PlannedTransaction } from '../data/plannedTransactionCollection';
import { BudgetModel, CategoryInfo } from './BudgetModel';
import { daysInYear } from '../utils/dateUtil';

// The sorting of these properties is important !
// Observed amount and count can be calculated from transactions, so processing transactions before the others has a
// performance impact.
enum BudgetModelProperties {
    transactions,
    observedAmount,
    plannedAmount,
    plannedAdjustedAmount,
    transactionCount,
}

type BudgetModelParams = {
    userId: string;
    modelYear: number;
    from?: dayjs.Dayjs;
    to?: dayjs.Dayjs;
    filter?: Record<string, any>;
    transactionIds?: string[];
};

type BudgetModelOptions = {
    forceMultiYearModeling?: boolean;
    ignoreUncategorized?: boolean;
};

export class BudgetModelBuilder {
    static create(config: BudgetModelParams, options?: BudgetModelOptions) {
        return new BudgetModelBuilder(config, options);
    }

    private readonly options?: BudgetModelOptions;
    private readonly userId: string;
    private readonly modelYear: number;
    private readonly originalFrom?: dayjs.Dayjs;
    private transactionsFrom?: dayjs.Dayjs;
    private readonly from?: dayjs.Dayjs;
    private readonly originalTo?: dayjs.Dayjs;
    private readonly to?: dayjs.Dayjs;
    private readonly transactionFilters: {
        filter?: Record<string, any>;
        transactionIds?: string[];
    };
    private requiredProperties: BudgetModelProperties[] = [];
    private rootCategoryIds: string[];
    public budget: BudgetDocument;
    public categories: LeanCategory[];
    private daysInRange: number;
    private transactionCountAggregate: { _id: string; count: number }[];
    private nTree: NTree<CategoryInfo>;

    private get propertyBuilderMapping() {
        return {
            [BudgetModelProperties.transactions]: () => this.attachTransactions(),
            [BudgetModelProperties.observedAmount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculateObservedAmount(nodes),
            [BudgetModelProperties.plannedAmount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculatePlannedAmount(nodes),
            [BudgetModelProperties.plannedAdjustedAmount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculatePlannedAdjustedAmount(nodes),
            [BudgetModelProperties.transactionCount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculateTransactionCount(nodes),
        };
    }

    private constructor(
        { userId, to, from, filter, transactionIds, modelYear }: BudgetModelParams,
        options?: BudgetModelOptions
    ) {
        this.options = options;
        const defaultFrom = dayjs().year(modelYear).startOf('year');
        const defaultTo = dayjs().year(modelYear).endOf('year');

        this.userId = userId;
        this.modelYear = modelYear;
        this.transactionFilters = {
            filter: filter,
            transactionIds: transactionIds,
        };

        this.originalFrom = from || defaultFrom;
        this.transactionsFrom = from || defaultFrom;
        this.originalTo = to || defaultTo;

        if (options?.forceMultiYearModeling) {
            this.from = this.originalFrom;
            this.to = this.originalTo;
        } else {
            // force the model to scope within the selected budget year
            this.from = from && from.isAfter(defaultFrom) ? from : defaultFrom;
            this.to = to && to.isBefore(defaultTo) ? to : defaultTo;
        }
    }

    appendTransactions(transactionsFrom?: dayjs.Dayjs) {
        if (transactionsFrom) {
            this.transactionsFrom = transactionsFrom;
        }
        this.requiredProperties.push(BudgetModelProperties.transactions);
        return this;
    }
    appendObservedAmount(transactionsFrom?: dayjs.Dayjs) {
        if (transactionsFrom) {
            this.transactionsFrom = transactionsFrom;
        }
        this.requiredProperties.push(BudgetModelProperties.transactions);
        this.requiredProperties.push(BudgetModelProperties.observedAmount);
        return this;
    }
    appendPlannedAmount() {
        this.requiredProperties.push(BudgetModelProperties.plannedAmount);
        return this;
    }
    appendPlannedAdjustedAmount() {
        this.requiredProperties.push(BudgetModelProperties.plannedAmount);
        this.requiredProperties.push(BudgetModelProperties.observedAmount);
        this.requiredProperties.push(BudgetModelProperties.plannedAdjustedAmount);
        return this;
    }
    appendTransactionCount() {
        this.requiredProperties.push(BudgetModelProperties.transactionCount);
        return this;
    }
    trimToRoots(rootCategoryIds: string[]) {
        this.rootCategoryIds = rootCategoryIds;
        return this;
    }
    async build() {
        this.budget = await Budget.getClosest(this.userId, this.modelYear, true);
        this.categories =
            (this.budget && (await getCategories(this.budget))) || (await Category.find().forUser(this.userId).lean());

        this.daysInRange = this.to.diff(this.from, 'day', true);
        this.nTree = new NTree(
            this.categories.map((category) => {
                return {
                    category,
                    budgetEntry: this.budget?.findEntryByCategory(category._id),
                };
            }),
            (c) => c.category.name,
            (c) => c.category.parent
        );

        if (this.rootCategoryIds) {
            this.trimAndRebuildTree();
        }

        for (let property of _.uniq(this.requiredProperties.sort())) {
            await this.propertyBuilderMapping[property](this.nTree.getRootNodes());
        }

        return new BudgetModel(
            this.userId,
            this.budget,
            this.nTree,
            this.categories,
            this.originalFrom,
            this.originalTo
        );
    }

    private trimAndRebuildTree() {
        const rootNodes: TreeNode<CategoryInfo>[] = [];
        this.nTree.iterateConditional((node) => {
            const { category } = node.value;
            if (this.rootCategoryIds.includes(category._id.toString())) {
                rootNodes.push(node);
            } else {
                return true;
            }
        });
        const nodeValues = _.chain(rootNodes)
            .forEach((node) => (node.value.category.parent = null))
            .map((node) => node.toArray())
            .flatten()
            .value();
        this.categories = nodeValues.map((info) => info.category);
        this.nTree = new NTree(
            nodeValues,
            (c) => c.category.name,
            (c) => c.category.parent
        );
    }

    private async attachTransactions() {
        const transactions = await searchTransactions({
            userId: this.userId,
            filter: _.extend(this.transactionFilters?.filter, {
                date_from: this.transactionsFrom.toDate(),
                date_to: this.to.toDate(),
            }),
            preSearchIds: this.transactionFilters?.transactionIds,
        });
        this.nTree.iterate((node) => {
            const { category } = node.value;
            if (category.type === CategoryType.base) {
                node.value.transactions = transactions.filter((t) => t.category === category.name);
            }
        });
        if (!this.options?.ignoreUncategorized) {
            this.attachUncategorizedTransactions(transactions);
        }
    }

    private attachUncategorizedTransactions(transactions: LeanTransaction[]) {
        const uncategorizedTransactions = transactions.filter((t) => !t.category);
        const [revenues, spending] = _.partition(uncategorizedTransactions, (t) => t.amount > 0);
        if (!_.isEmpty(revenues)) {
            this.nTree.add({
                transactions: revenues,
                category: new Category({
                    name: '(Revenues sans catégorie)',
                    type: CategoryType.base,
                    systemType: SystemCategory.income,
                    userId: this.userId,
                }),
            });
        }
        if (!_.isEmpty(spending)) {
            this.nTree.add({
                transactions: spending,
                category: new Category({
                    name: '(Dépenses sans catégorie)',
                    type: CategoryType.base,
                    systemType: null,
                    userId: this.userId,
                }),
            });
        }
    }

    private async calculateObservedAmount(nodes: TreeNode<CategoryInfo>[]) {
        for (let node of nodes) {
            const { transactions } = node.value;
            if (node.value.category.type === CategoryType.base) {
                if (_.isArray(transactions)) {
                    node.value.observedAmount = _.sumBy(transactions, 'amount');
                }
            } else {
                await this.calculateObservedAmount(node.children);
                node.value.observedAmount = _.sumBy(node.children, (node) => node.value.observedAmount);
            }
        }
    }

    private async calculatePlannedAmount(nodes: TreeNode<CategoryInfo>[]) {
        this.nTree.iterateConditional((node) => {
            const { budgetEntry } = node.value;
            if (budgetEntry && !budgetEntry.isComplex) {
                const categories = node.toArray().map((node) => node.category);
                node.value.plannedAmountPerDay = budgetEntry.calculatePlannedBudgetSumPerDay(categories);
            } else {
                return true;
            }
        });
        await this.attachPlannedTransactions();
        this.combinePlannedAmounts(nodes);
    }

    private async attachPlannedTransactions() {
        if (!this.budget) {
            return;
        }

        // If the model is for a futur year, this array will be empty.
        const plannedTransactions = await PlannedTransaction.find({
            budgetEntryId: {
                $in: this.budget.entries.map((e) => e._id.toString()),
            },
            date: {
                $gte: this.from.toDate(),
                $lte: this.to.toDate(),
            },
        });

        this.nTree.iterate((node) => {
            const { budgetEntry, category } = node.value;
            if (budgetEntry?.isComplex && category) {
                if (this.modelYear > this.budget.year) {
                    node.value.plannedTransactions = budgetEntry
                        .getPlannedTransactions(this.from, this.to)
                        .map((transaction) => {
                            if (category.systemType !== SystemCategory.income) {
                                transaction.amount *= -1;
                            }
                            return transaction;
                        });
                } else {
                    node.value.plannedTransactions = plannedTransactions
                        .filter((pt) => pt.budgetEntryId.equals(budgetEntry._id))
                        .map((pt) => {
                            if (category.systemType !== SystemCategory.income) {
                                pt.amount *= -1;
                            }
                            return pt;
                        });
                }
                if (this.originalTo.isAfter(this.to)) {
                    node.value.extendedPlannedTransactions = budgetEntry
                        .getPlannedTransactions(this.to, this.originalTo)
                        .map((transaction) => {
                            if (category.systemType !== SystemCategory.income) {
                                transaction.amount *= -1;
                            }
                            return transaction;
                        });
                } else {
                    node.value.extendedPlannedTransactions = [];
                }
            }
        });
    }

    private combinePlannedAmounts(nodes: TreeNode<CategoryInfo>[]) {
        for (let node of nodes) {
            const { budgetEntry, plannedTransactions } = node.value;
            if (budgetEntry && !budgetEntry.isComplex) {
                node.value.plannedAmount = Math.abs(this.daysInRange * node.value.plannedAmountPerDay);
            } else if (plannedTransactions) {
                node.value.plannedAmount = Math.abs(_.sumBy(plannedTransactions, (transaction) => transaction.amount));
            } else {
                this.combinePlannedAmounts(node.children);
                node.value.plannedAmount = _.sumBy(node.children, (node) => node.value.plannedAmount);
            }
        }
    }

    private async calculatePlannedAdjustedAmount(nodes: TreeNode<CategoryInfo>[]) {
        const isCurrentOrFutureYear = this.from.year() >= dayjs().year();
        const dateFrom = isCurrentOrFutureYear
            ? dayjs().subtract(1, 'year')
            : dayjs().year(this.from.year()).startOf('year');
        const dateTo = isCurrentOrFutureYear ? dayjs() : dayjs().year(this.from.year()).endOf('year');
        const searchConfig = {
            userId: this.userId,
            filter: {
                date_from: dateFrom.toDate().toISOString(),
                date_to: dateTo.toDate().toISOString(),
            },
        };
        const transactions = isCurrentOrFutureYear ? await searchTransactions(searchConfig) : [];
        this.sumUpPlannedAdjustedAmount(nodes, transactions);
    }

    private sumUpPlannedAdjustedAmount(nodes: TreeNode<CategoryInfo>[], transactions: LeanTransaction[]) {
        for (let node of nodes) {
            const { category, plannedTransactions, budgetEntry } = node.value;
            if (category.type === CategoryType.base) {
                const sumPerDay =
                    _.chain(transactions)
                        .filter((t) => t.category === category.name)
                        .sumBy((t) => t.amount)
                        .value() / daysInYear(this.from.year());
                const plannedSum = _.chain(plannedTransactions)
                    // As to not sum the count transaction twice (planned + observed), we filter out the ones in the past.
                    // FIXME: This is quite imprecise...
                    .filter((t) => new Date(t.date).getTime() >= Date.now())
                    .sumBy((t) => t.amount)
                    .value();

                if (this.from.year() < dayjs().year()) {
                    // past
                    node.value.plannedAdjustedAmount = Math.abs(node.value.observedAmount);
                } else if (this.from.year() === dayjs().year()) {
                    // present
                    const daysInRange = this.to.diff(dayjs(), 'day', true);
                    node.value.plannedAdjustedAmount =
                        budgetEntry && budgetEntry.isComplex
                            ? Math.abs(node.value.observedAmount) + Math.abs(plannedSum)
                            : Math.abs(node.value.observedAmount) + Math.abs(daysInRange * sumPerDay);
                } else {
                    // future
                    node.value.plannedAdjustedAmount =
                        budgetEntry && budgetEntry.isComplex
                            ? Math.abs(plannedSum)
                            : Math.abs(this.daysInRange * sumPerDay);
                }
            } else {
                this.sumUpPlannedAdjustedAmount(node.children, transactions);
                node.value.plannedAdjustedAmount = _.sumBy(node.children, (node) => node.value.plannedAdjustedAmount);
            }
        }
    }

    private async calculateTransactionCount(nodes: TreeNode<CategoryInfo>[]) {
        for (let node of nodes) {
            const { category, transactions } = node.value;
            if (category.type === CategoryType.base) {
                node.value.transactionCount = _.isArray(transactions)
                    ? transactions.length
                    : (await this.getTransactionCountAggregate()).find((aggregate) => aggregate._id === category.name)
                          ?.count || 0;
            } else {
                await this.calculateTransactionCount(node.children);
                node.value.transactionCount = _.sumBy(node.children, (node) => node.value.transactionCount);
            }
        }
    }

    private async getTransactionCountAggregate() {
        if (!this.transactionCountAggregate) {
            this.transactionCountAggregate = await Transaction.countByCategories(
                this.userId,
                this.categories
                    .filter((category) => category.type === CategoryType.base)
                    .map((category) => category.name),
                this.transactionsFrom.toDate(),
                this.originalTo.toDate()
            );
        }
        return this.transactionCountAggregate;
    }
}
