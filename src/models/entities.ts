import { AttachmentType, CategoryType, SystemCategory, GoalNecessity, StatementFileType } from './enums';
import { Types } from 'mongoose';
import { DateInterval, SimpleDateInterval } from '../utils/dateUtil';

export type Transaction = {
    id: string;
    description: string;
    descriptionInfo?: TransactionDescriptionInfo;
    date: Date;
    amount: number;
    currency: string;
    conversionRate: number;
    creationDate: Date;
    statementId: string;
    account: string;
    category?: string;
    attachments: Array<TransactionAttachment>;
    categoryHierarchy: Array<CategoryHierarchyItem>;
    isPending: boolean;
};

export type EssentialTransactionData = {
    description: string;
    date: Date;
    amount: number;
    account?: string;
    category?: string;
    currency?: string;
    conversionRate?: number;
};

export type TransactionDescriptionInfo = {
    type?: string;
    main?: string;
    location?: string;
};

export type TransactionAttachment = {
    fileId: string;
    type: AttachmentType;
    filename: string;
};

export type TransactionEsEntity = {
    id: string;
    creationDate: Date;
    conversionRate: number;
    amount: number;
    currency: string;
    description?: string;
    type?: string;
    location?: string;
    descriptionMain?: string;
    date: Date;
    category?: string;
    statement: string;
    userId: string;
    ignoreForAutoCategoryAssignment: boolean;
};

export type CategoryHierarchyItem = {
    _id: string;
    name: string;
    hue: string;
    systemType: SystemCategory;
    type: CategoryType;
};

export type Goal = {
    id: string;
    name: string;
    amount: number;
    description?: string;
    necessity?: GoalNecessity;
    deadline?: Date;
    transactions?: string[];
    finalAmount?: number;
    completionDate?: Date;
};

export type Category = {
    id?: string;
    name: string;
    creationDate: Date;
    type: CategoryType;
    systemType: SystemCategory;
    hue?: string;
    parent?: string;
    children?: Array<any>;
    matchedDescriptions?: Array<string>;
};

export type Statement = {
    id: string;
    date: Date;
    creationDate: Date;
    fileId?: string;
    fileType: StatementFileType;
    isApproved?: boolean;
};

export type Account = {
    id: string;
    amount: number;
    accountName: string;
    accountType: string;
    updatedAt: Date;
    creationDate: Date;
    isInvestmentAccount: boolean;
    displayName?: string;
    creditLimit?: number;
    disabled?: boolean;
};

export type Budget = {
    id: string;
    isClosed: boolean;
    year: number;
    hasBudget: boolean;
    budgetEntries: BudgetEntry[];
    rootCategories?: Category[];
};
export type BudgetEntry = {
    _id?: string;
    categoryId: Types.ObjectId;
    isComplex: boolean;
    // fields for simple budget
    amount: number;
    period: SimpleDateInterval;
    // fields for complex budget
    preset: string;
    rules: [
        {
            period: DateInterval;
            amount: number;
            startDate: Date;
            endDate: Date;
        }
    ];
};

export type BalanceHistoryDataset = [number, number][];
