import { BudgetDocument } from '../data/budgetCollection';
import { LeanCategory } from '../data/categoryCollection';
import NTree, { TreeNode } from './nTree';
import * as dayjs from 'dayjs';
import plotDataBuilder from './plotDataBuilder';
import { CategoryType, isBaseNonTransfer, isExpenseSystemCategory } from './enums';
import { LeanTransaction, Transaction } from '../data/transactionCollection';
import { daysInYear } from '../utils/dateUtil';
import { BudgetEntryDocument, VirtualPlannedTransaction } from '../data/budgetEntryCollection';
import { HydratedPlannedTransaction } from '../data/plannedTransactionCollection';

export type BudgetModelType = BudgetModel;

export type CategoryInfo = {
    category: LeanCategory;
    budgetEntry?: BudgetEntryDocument;
    transactions?: LeanTransaction[];
    plannedTransactions?: Array<VirtualPlannedTransaction | HydratedPlannedTransaction>;
    extendedPlannedTransactions?: VirtualPlannedTransaction[];
    plannedAmountPerDay?: number;
    plannedAmount?: number;
    plannedAdjustedAmount?: number;
    observedAmount?: number;
    transactionCount?: number;
};

export class BudgetModel {
    public budget: BudgetDocument;
    public categories: LeanCategory[];
    private readonly userId: string;
    private readonly nTree: NTree<CategoryInfo>;
    private readonly from: dayjs.Dayjs;
    private readonly to: dayjs.Dayjs;

    constructor(
        userId: string,
        budget: BudgetDocument,
        nTree: NTree<CategoryInfo>,
        categories: LeanCategory[],
        from: dayjs.Dayjs,
        to: dayjs.Dayjs
    ) {
        this.userId = userId;
        this.budget = budget;
        this.nTree = nTree;
        this.categories = categories;
        this.from = from;
        this.to = to;
    }

    getRootNodes() {
        return this.nTree.getRootNodes();
    }

    getCategoryHierarchy<T>(propertySelector: (element: CategoryInfo) => T) {
        return this.nTree.getCategoryHierarchy(propertySelector);
    }

    findNode(searchFn: (node: TreeNode<CategoryInfo>) => boolean) {
        return this.nTree.findNode(searchFn);
    }

    iterate(predicate: (node: TreeNode<CategoryInfo>) => void) {
        return this.nTree.iterate(predicate);
    }

    getGlobalPlannedSumParDay() {
        let globalSum = 0;
        this.nTree.iterateConditional((node) => {
            const { budgetEntry, plannedAmountPerDay } = node.value;
            if (budgetEntry && !budgetEntry.isComplex) {
                globalSum += plannedAmountPerDay;
            } else {
                return true;
            }
        });
        return globalSum;
    }

    buildPlannedPlotData(granularity: string) {
        if (!this.budget) {
            return [];
        }

        const daysInRange = this.to.diff(this.from, 'day', true);
        const { sumPerDay, plannedTransactions, extendedPlannedTransactions } = this.getPlannedData();
        return plotDataBuilder
            .create({
                from: this.from,
                to: this.to,
                interval: granularity,
            })
            .addTransactions(plannedTransactions)
            .addTransactions(extendedPlannedTransactions)
            .addAmount(daysInRange * sumPerDay)
            .value();
    }

    async buildPlannedAdjustedPlotData(granularity: string) {
        if (!this.budget) {
            return [];
        }

        const recurringOrIgnoredSubCategoryIds = (this.budget?.entries || [])
            .filter((entry) => entry.isComplex || entry.amount === 0) // FIXME: We have to exclude some categories with a
            //                                                                     "zero amount budget" because those categories
            //                                                                     had a complex budget last year but we know wont
            //                                                                     have any transactions this year. Fix this.
            .map((entry) => entry.categoryId);
        const nonRecurringNonTransferSubCategoryNames = this.categories
            .filter(
                (_cat) =>
                    isBaseNonTransfer(_cat) && !recurringOrIgnoredSubCategoryIds.some((_id) => _id.equals(_cat._id))
            )
            .map((_cat) => _cat.name);
        const sumForAYear = await Transaction.sumAmountsForSubCategories(
            this.userId,
            nonRecurringNonTransferSubCategoryNames,
            this.from.year() >= dayjs().year()
                ? dayjs().subtract(1, 'year').toDate()
                : dayjs().year(this.from.year()).startOf('year').toDate(),
            this.from.year() >= dayjs().year()
                ? dayjs().toDate()
                : dayjs().year(this.from.year()).endOf('year').toDate()
        );
        const sumPerDay = sumForAYear / daysInYear(this.budget.year);

        const daysInRange = this.to.diff(this.from, 'day', true);
        const { plannedTransactions, extendedPlannedTransactions } = this.getPlannedData();
        return plotDataBuilder
            .create({
                from: this.from,
                to: this.to,
                interval: granularity,
            })
            .addTransactions(plannedTransactions)
            .addTransactions(extendedPlannedTransactions)
            .addAmount(daysInRange * sumPerDay)
            .value();
    }

    private getPlannedData() {
        let sumPerDay = 0;
        const _plannedTransactions: VirtualPlannedTransaction[] = [];
        const _extendedPlannedTransactions: VirtualPlannedTransaction[] = [];
        this.nTree.iterateConditional((node) => {
            const { budgetEntry, plannedTransactions, extendedPlannedTransactions, plannedAmountPerDay } = node.value;
            if (plannedTransactions || extendedPlannedTransactions) {
                _plannedTransactions.push(...plannedTransactions);
                _extendedPlannedTransactions.push(...extendedPlannedTransactions);
            } else if (budgetEntry && !budgetEntry.isComplex) {
                sumPerDay += plannedAmountPerDay;
            } else {
                return true;
            }
        });
        return {
            sumPerDay,
            plannedTransactions: _plannedTransactions,
            extendedPlannedTransactions: _extendedPlannedTransactions,
        };
    }

    getUnplannedObservedAmount() {
        let sum = 0;
        this.nTree.iterateConditional((node) => {
            const { budgetEntry, observedAmount, category } = node.value;
            if (budgetEntry || !isExpenseSystemCategory(category.systemType)) {
                return false;
            }
            if (category.type === CategoryType.base) {
                sum += observedAmount || 0;
            }
            return true;
        });
        return sum;
    }
}
