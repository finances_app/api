import { sumBy } from 'lodash';
import { Dayjs } from 'dayjs';
import { VirtualPlannedTransaction } from '../data/budgetEntryCollection';

export default class plotDataBuilder {
    static create({ from, to, interval }: { from: Dayjs; to: Dayjs; interval: string }) {
        return new plotDataBuilder(from, to, interval);
    }

    private readonly dataset: Array<[number, number]>;
    private readonly from: Dayjs;
    private readonly to: Dayjs;
    private readonly interval: string;
    private readonly intervalCount: number;

    private constructor(from: Dayjs, to: Dayjs, interval: string) {
        this.dataset = [];
        this.from = from;
        this.to = to;
        const [first, second] = interval.split('-');
        this.interval = second ? second : first;
        this.intervalCount = second ? Number(first) : 1;

        // having the cursor start there will make sure the interval between multiple plotdata line up
        let dateCursor = from.clone().startOf('year');
        let isBeforeStart = true;
        while (!dateCursor.isAfter(to)) {
            if (!isBeforeStart) {
                this.dataset.push([dateCursor.valueOf(), 0]);
            } else {
                // counter-intuitive code to make sure the first datum starts exactly at 'from'
                // the first datum will most likely be of a shorter timespan than the others
                isBeforeStart = dateCursor.isBefore(from);
                if (!isBeforeStart) {
                    this.dataset.push([from.valueOf(), 0]);
                }
            }
            // @ts-ignore
            dateCursor = dateCursor.add(this.intervalCount, this.interval);
        }
    }

    addTransactions(transactions: VirtualPlannedTransaction[]) {
        for (let [index, tuple] of this.dataset.entries()) {
            if (index === 0) {
                continue;
            }

            const previousTuple = this.dataset[index - 1];
            const from = previousTuple && previousTuple[0];
            const to = tuple[0];

            const generatedTransactionsWithinPeriod = transactions.filter((_transaction) => {
                const transactionDate = _transaction.date.getTime();
                return from <= transactionDate && transactionDate < to;
            });
            const recurringTransactionSum = sumBy(
                generatedTransactionsWithinPeriod,
                (_transaction) => _transaction.amount
            );

            tuple[1] += recurringTransactionSum;
        }

        return this;
    }

    addAmount(amount: number) {
        // @ts-ignore
        const plotDataCount = this.to.diff(this.from, this.interval, true) / this.intervalCount;
        const amountPerInterval = amount / plotDataCount;

        for (let tuple of this.dataset) {
            tuple[1] += amountPerInterval;
        }

        return this;
    }

    value() {
        for (let [index, tuple] of this.dataset.entries()) {
            if (index === 0) {
                tuple[1] = 0;
            } else {
                const previousTuple = this.dataset[index - 1];
                tuple[1] += previousTuple[1];
            }
        }
        return this.dataset;
    }
}
