/**
 * Created by hugo on 2018-12-29
 */
import { HydratedDocument, Model, Schema, model, Types } from 'mongoose';
import { uniq } from 'lodash';
import { UserLevel, AuthenticationType } from '../models/enums';

export interface IUserDevice {
    _id?: Types.ObjectId;
    platform?: string;
    manufacturer?: string;
    model?: string;
    uuid?: string;
    version?: string;
    lastLocation?: string;
    timestamp?: number;
    refreshToken?: string;
    deviceToken?: string;
    pendingCacheExpirationEvents?: Array<string>;
    verified?: boolean;
    ip?: string;
}

export interface IUserLocation {
    _id?: Types.ObjectId;
    country?: string;
    region?: string;
    city?: string;
    ll?: [number, number];
    ip?: string;
    timestamp?: number;
}

export interface IApiAccess {
    _id?: Types.ObjectId;
    creationDate?: Date;
    apiKey: string;
    lastActivity?: Date;
    disabled?: boolean;
}

export interface IUser {
    googleUserId: string;
    email?: string;
    givenName?: string;
    familyName?: string;
    creationDate: Date;
    lastLogin?: Date;
    level: UserLevel;
    authenticationType?: AuthenticationType;
    locations: [IUserLocation];
    devices: [IUserDevice];
    apiAccess: [IApiAccess];
}

type UserData = {
    sub: string;
    email?: string;
    given_name?: string;
    family_name?: string;
};

interface IUserMethods {
    getDevice(deviceToken: string): IUserDevice;
    getLocation(ip: string): IUserLocation;
    addCacheExpirationEvents(events: string[]): Promise<void>;
}

interface UserDocument extends IUser, Document, IUserMethods {}
export type HydratedUser = HydratedDocument<UserDocument>;

interface UserModel extends Model<UserDocument, {}, IUserMethods> {
    createIfNotExist(payload: UserData): Promise<HydratedUser>;
}

let userSchema = new Schema<UserDocument, UserModel, IUserMethods>({
    googleUserId: { type: String, unique: true },
    email: String,
    givenName: String,
    familyName: String,
    creationDate: { type: Schema.Types.Date, default: Date.now },
    lastLogin: Schema.Types.Date,
    level: { type: String, enum: [UserLevel.user, UserLevel.admin, UserLevel.readOnly], default: UserLevel.user },
    authenticationType: {
        type: String,
        enum: [AuthenticationType.demo, AuthenticationType.google, AuthenticationType.local],
    },
    locations: [
        {
            country: String,
            region: String,
            city: String,
            ll: [Number], // this is longitude - latitude coordinates
            ip: String,
            timestamp: Number,
        },
    ],
    devices: [
        {
            platform: String,
            manufacturer: String,
            model: String,
            uuid: String,
            version: String,
            lastLocation: String,
            timestamp: Number,
            refreshToken: String,
            deviceToken: String,
            pendingCacheExpirationEvents: { type: [String], default: [] },
            verified: Boolean,
            ip: String,
        },
    ],
    apiAccess: [
        {
            creationDate: { type: Schema.Types.Date, default: Date.now },
            apiKey: String,
            lastActivity: Schema.Types.Date,
            disabled: Boolean,
        },
    ],
});

userSchema.method('getDevice', function getDevice(this: HydratedUser, deviceToken: string) {
    return this.devices.find((device) => device.deviceToken === deviceToken);
});

userSchema.method('getLocation', function getLocation(this: HydratedUser, ip: string) {
    return this.locations.find((location) => location.ip === ip);
});

userSchema.method('addCacheExpirationEvents', async function addCacheExpirationEvents(
    this: HydratedUser,
    events: string[]
) {
    let updatedDevices = [];
    for (let device of this.devices) {
        device.pendingCacheExpirationEvents = uniq([...device.pendingCacheExpirationEvents, ...events]);
        updatedDevices.push(device);
    }
    await this.updateOne({ googleUserId: this.googleUserId }, { $set: { devices: updatedDevices } });
});

userSchema.static('createIfNotExist', async function createIfNotExist(
    this: UserModel,
    payload: UserData
): Promise<HydratedUser> {
    const { sub, email, given_name, family_name } = payload;
    await this.findOneAndUpdate(
        { googleUserId: sub },
        {
            $set: {
                googleUserId: sub,
                email: email,
                givenName: given_name,
                familyName: family_name,
                lastLogin: Date.now(),
            },
        },
        { upsert: true }
    );
    return this.findOne({ googleUserId: sub });
});

export { userSchema };
export const User = model<UserDocument, UserModel>('user', userSchema);
