import * as dayjs from 'dayjs';
import { Schema, Model, Document, Types } from 'mongoose';
import { addOnePeriod, DateInterval, SimpleDateInterval, periodsInYear, daysInYear } from '../utils/dateUtil';
import { HydratedCategory, LeanCategory } from './categoryCollection';
import { SystemCategory } from '../models/enums';
import { BudgetEntry as BudgetEntryDto } from '../models/entities';

export type VirtualPlannedTransaction = {
    amount: number;
    date: Date;
};

export interface IBudgetEntry {
    categoryId: Types.ObjectId;
    isComplex: boolean;
    // fields for simple budget
    amount: number;
    period: SimpleDateInterval;
    // fields for complex budget
    preset: string;
    rules: [
        {
            period: DateInterval;
            amount: number;
            startDate: Date;
            endDate: Date;
        }
    ];
}

export type BudgetEntryDocument = IBudgetEntry & Document & IBudgetEntryMethods;

interface BudgetEntryModel extends Model<BudgetEntryDocument, {}, IBudgetEntryMethods> {}

let budgetEntrySchema = new Schema<BudgetEntryDocument, BudgetEntryModel, IBudgetEntryMethods>({
    // @ts-ignore
    categoryId: { type: Types.ObjectId, required: true },
    isComplex: Boolean,
    // fields for simple budget
    amount: Number,
    period: { type: String, enum: ['w', 'M', 't', 'y'] },
    // fields for complex budget
    preset: String,
    rules: [
        {
            period: { type: String, enum: ['w', '2w', 'M', 't', 'q', 'y'] },
            amount: Number,
            startDate: Schema.Types.Date,
            endDate: Schema.Types.Date,
        },
    ],
});

interface IBudgetEntryMethods {
    toDto(): BudgetEntryDto;
    getPlannedTransactions(minDate: dayjs.Dayjs, maxDate: dayjs.Dayjs): Array<VirtualPlannedTransaction>;
    calculatePlannedBudgetSumPerDay(categories: Array<HydratedCategory | LeanCategory>): number | null;
    getMaximumSloppinessInDays(): number;
}

budgetEntrySchema.method('toDto', function toDto(this: BudgetEntryDocument): BudgetEntryDto {
    return {
        _id: this._id?.toString() || this.id,
        amount: this.amount,
        categoryId: this.categoryId,
        isComplex: this.isComplex,
        period: this.period,
        preset: this.preset,
        rules: this.rules,
    };
});

budgetEntrySchema.method('getPlannedTransactions', function getPlannedTransactions(
    minDate: dayjs.Dayjs,
    maxDate: dayjs.Dayjs
): Array<VirtualPlannedTransaction> {
    let plannedTransactions: Array<VirtualPlannedTransaction> = [];

    if (!this.isComplex) {
        return plannedTransactions;
    }

    for (let rule of this.rules.values()) {
        if (!rule.period) {
            let startDateTimestamp = dayjs(rule.startDate).valueOf();
            if (minDate.valueOf() <= startDateTimestamp && startDateTimestamp <= maxDate.valueOf()) {
                plannedTransactions.push({ amount: rule.amount, date: dayjs(rule.startDate).toDate() });
            }
        } else {
            let startDate = dayjs(rule.startDate);
            while (startDate.isBefore(minDate)) {
                startDate = addOnePeriod(startDate, rule.period);
            }

            let endDate = rule.endDate && dayjs(rule.endDate);
            endDate = !endDate || endDate.isAfter(maxDate) ? maxDate.clone() : endDate;

            // Move the startDate forward by the specified period until it goes past the endDate
            while (startDate.isBefore(endDate)) {
                plannedTransactions.push({
                    amount: rule.amount,
                    date: startDate.toDate(),
                });

                startDate = addOnePeriod(startDate, rule.period);
            }
        }
    }

    return plannedTransactions;
});

budgetEntrySchema.method('calculatePlannedBudgetSumPerDay', function calculatePlannedBudgetSumPerDay(
    categories: Array<HydratedCategory | LeanCategory>
): number | null {
    if (this.isComplex) {
        return null;
    }

    const budget = this.parent();

    const category = categories.find((_cat) => _cat._id.equals(this.categoryId));
    if (!category) {
        throw new Error(
            `The budget ${budget.year} for user ${budget.userId} has a budget entry referencing the category ` +
                `${this.categoryId.toString()}, but the category does not exist either in the ` +
                `categoriesSnapshot or the category collection.`
        );
    }

    const timesInYear = periodsInYear(this.period);
    if (timesInYear === null) {
        throw new Error(`Budget entry with invalid period '${this.period}'.`);
    }
    let sumPerDay = (this.amount * timesInYear) / daysInYear(budget.year);

    const sumCalculationMapping: Record<SystemCategory | 'default', (sumPerDay: number) => number> = {
        [SystemCategory.income]: (sumParDay) => sumParDay,
        [SystemCategory.transfer]: () => 0,
        [SystemCategory.goal]: (sumParDay) => sumParDay * -1,
        default: (sumParDay) => sumParDay * -1,
    };

    if (sumCalculationMapping[category.systemType]) {
        return sumCalculationMapping[category.systemType](sumPerDay);
    }
    return sumCalculationMapping.default(sumPerDay);
});

budgetEntrySchema.method('getMaximumSloppinessInDays', function getMaximumSloppinessInDays(
    this: BudgetEntryDocument
): number {
    // There can be some slop in the date matching on the actual transaction side.
    // This is because bank typicaly dont record transactions on week-ends and some holidays.
    // Adding to the complexity, transactions occuring on off-days will randomly be assigned the first
    // business day before OR after the off-day period.

    // Actually this all goes out of the window because we may want to track some recurring transactions
    // that are handled by a human ! Paychecks is a good example. They will occur on average every 2 weeks
    // lets say, but one could forget to send one for a while. This could result in two pay checks being
    // sent nearly on the same day.
    // We should therefore accept as much sloppiness as possible.

    // There is no rhyme or reason behind these numbers, they are basically magic numbers.
    return Math.max(
        ...this.rules.map((rule) => {
            // prettier-ignore
            switch (rule.period) {
                case 'w':   return 3;
                case '2w':  return 5;
                case 'M':   return 10;
                case 'q':   return 25;
                case 't':   return 35;
                case 'y':   return 40;
                default:    return 5;
            }
        })
    );
});

export { budgetEntrySchema };
