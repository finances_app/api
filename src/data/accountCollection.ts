import { AccountType } from '../models/enums';
import { HydratedDocument, Model, Schema, Document, model, LeanDocument } from 'mongoose';
import { GenericQueryHelpers, IGenericSchema, GenericSchema, forUser } from './genericModel';
import { extend  } from 'lodash';
import { Account as AccountDto } from '../models/entities';

export interface IAccount extends IGenericSchema {
    amount: number;
    creditLimit?: number;
    accountName: string;
    displayName?: string;
    accountType: string;
    isInvestmentAccount: boolean;
    recordCount: number;
    pendingRecords: any[];
    updatedAt: Date;
    creationDate: Date;
    disabled?: boolean;
}

export interface IAccountMethods {
    toDto(): AccountDto;
}

interface AccountDocument extends IAccount, Document, IAccountMethods {}
export type LeanAccount = LeanDocument<AccountDocument>;
export type HydratedAccount = HydratedDocument<AccountDocument>;

interface AccountModel extends Model<AccountDocument, GenericQueryHelpers<AccountDocument>, IAccountMethods> {
    createOrUpdate(account: HydratedAccount, userId: string): Promise<HydratedAccount>;
    getActiveAccounts(userId: string): Promise<HydratedAccount[]>;
    toDto(account: LeanAccount): AccountDto;
}

let accountSchema = new Schema<
    AccountDocument,
    AccountModel,
    IAccountMethods,
    GenericQueryHelpers<AccountDocument>
>(
    extend(
        {
            amount: { required: true, type: Number },
            creditLimit: Number,
            accountName: { required: true, type: String },
            displayName: { type: String },
            accountType: { required: true, type: String },
            isInvestmentAccount: { type: Boolean, default: false },
            recordCount: { type: Number, default: 0 },
            pendingRecords: [Schema.Types.Mixed],
            updatedAt: { type: Schema.Types.Date, default: Date.now },
            creationDate: { type: Schema.Types.Date, default: Date.now },
            disabled: Boolean,
        },
        GenericSchema
    )
);

accountSchema.query.forUser = forUser<AccountDocument>;

accountSchema.method('toDto', function toDto(): AccountDto {
    return Account.toDto(this);
});

accountSchema.static('toDto', function toDto(account: LeanAccount): AccountDto {
    return {
        id: account._id.toString() || account.id,
        amount: account.amount,
        creditLimit: account.creditLimit,
        accountName: account.accountName,
        accountType: account.accountType,
        updatedAt: account.updatedAt,
        creationDate: account.creationDate,
        isInvestmentAccount: account.isInvestmentAccount,
        displayName: account.displayName || account.accountName,
        disabled: account.disabled || false,
    }
});

accountSchema.static('createOrUpdate', async function createOrUpdate(account: HydratedAccount, userId: string) {
    let existingAccount: HydratedAccount;
    if (account._id) {
        let results = await this.find({ _id: account._id }).forUser(userId);
        existingAccount = results[0];
        if (!existingAccount) {
            throw new Error(`No account with id ${account._id}`);
        }
    } else {
        let results = await this.find({
            accountName: account.accountName,
            ...(account.isInvestmentAccount
                ? { isInvestmentAccount: account.isInvestmentAccount }
                : { $or: [{ $exists: { isInvestmentAccount: false } }, { isInvestmentAccount: false }] }),
        })
            .forUser(userId);
        existingAccount = results[0];
    }

    if (existingAccount) {
        existingAccount.amount = account.amount;
        existingAccount.creditLimit = account.creditLimit;
        existingAccount.accountName = account.accountName;
        existingAccount.accountType = account.accountType;
        existingAccount.recordCount += account.recordCount || 0;
        existingAccount.pendingRecords = account.pendingRecords || [];
        existingAccount.updatedAt = account.updatedAt || new Date();

        await existingAccount.save();
        return existingAccount;
    } else {
        let res = await this.insertMany([
            {
                ...account,
                updatedAt: account.updatedAt || new Date(),
                pendingRecords: account.pendingRecords || [],
                userId,
            },
        ]);
        return res[0];
    }
});

accountSchema.static('getActiveAccounts', async function getActiveAccounts(userId) {
    return this.find({
        accountType: { $ne: AccountType.PR },
        isInvestmentAccount: { $ne: true },
        disabled: { $ne: true }
    })
        .forUser(userId);
});

export { accountSchema };
export const Account = model<AccountDocument, AccountModel>('account', accountSchema);
