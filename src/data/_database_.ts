/**
 * Created by hugo on 2018-05-13.
 */

import mongoose, { connection } from 'mongoose';
import * as logger from '../utils/simpleLogger';
import { esExists, esInitialize, reindex } from './elasticsearch/elasticsearch';
import * as config from 'config';
const SystemDataManager = require('../data/migration/SystemDataManager');

mongoose.Promise = global.Promise;
mongoose
    .connect(`${config.get('DATA.MONGODB_HOST')}${config.get('INSTANCE')}`, {})
    .catch((err: any) => logger.error(err));

export async function untilDatabaseConnection() {
    return new Promise<void>((resolve, reject) => {
        console.log(`Connecting to database..`);
        connection.on('error', (err: Error) => reject(err));
        connection.once('open', () => resolve());
    });
}

export async function initializeData() {
    console.log(`Initializing Elasticsearch indices..`);
    let doesExists = await esExists();
    if (doesExists.body === false) {
        await esInitialize();
    }

    if (process.env.NODE_ENV === 'CI') {
        console.log(`Skipping data migration`);
    } else {
        console.log(`Initializing system data..`);
        await SystemDataManager.initData();
        await SystemDataManager.migrateData();
    }

    if (process.env.NODE_ENV === 'PROD') {
        await reindex();
    }
}
