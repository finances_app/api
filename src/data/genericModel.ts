import { QueryWithHelpers, HydratedDocument } from 'mongoose';

export interface GenericQueryHelpers<SchemaInterface> {
    forUser<TResult = HydratedDocument<SchemaInterface>[]>(
        userId: string
    ): QueryWithHelpers<TResult, HydratedDocument<SchemaInterface>, GenericQueryHelpers<SchemaInterface>>;
}

export interface IGenericSchema {
    userId: string;
}

export const GenericSchema = {
    userId: { type: String, required: true },
};

export function forUser<SchemaInterface>(
    this: QueryWithHelpers<any, HydratedDocument<SchemaInterface>, GenericQueryHelpers<SchemaInterface>>,
    userId: string
) {
    return this.where({ userId });
}
