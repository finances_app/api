/**
 * @class AbstractMigrationScript
 * @abstract
 */
class AbstractMigrationScript {
    static get executeOnce() {
        return true;
    }

    /**
     * @returns {Promise<void>}
     * @abstract
     */
    static async migrate() {
        throw new Error('Non-implemented method');
    }
}
module.exports = AbstractMigrationScript;
