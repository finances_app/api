const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Budget } = require('../../budgetCollection');
const { replacePlannedTransactions } = require('../../../_resources_/budget/budgetController');
const { Types } = require('mongoose');

class InitializedPlannedTransactions extends AbstractMigrationScript {
    static async migrate() {
        const budgets = await Budget.find().exec();
        for (const budget of budgets) {
            for (const entry of budget.entries) {
                entry._id = new Types.ObjectId();
            }
            budget.markModified('entries');
            await budget.save();
        }

        for (const budget of budgets) {
            for (const entry of budget.entries) {
                await replacePlannedTransactions(budget.year, entry);
            }
        }
    }
}
module.exports = InitializedPlannedTransactions;
