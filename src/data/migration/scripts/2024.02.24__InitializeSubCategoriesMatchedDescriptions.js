const AbstractMigrationScript = require('../AbstractMigrationScript');
const { User } = require('../../userCollection');
const { Budget } = require('../../budgetCollection');
const { Transaction } = require('../../transactionCollection');
const AggregationBuilder = require('../../../models/AggregationBuilder').default;
const dayjs = require('dayjs');
const { Category } = require('../../categoryCollection');

class InitializeSubCategoriesMatchedDescriptions extends AbstractMigrationScript {
    static async migrate() {
        for (let user of await User.find()) {
            const budgets = await Budget.find({ userId: user.googleUserId }).sort({ year: 1 });

            for (let budget of budgets) {
                const isFirstBudget = budget.year === budgets[0].year;
                const matchQuery = {
                    userId: await user.googleUserId,
                    ignoreForAutoCategoryAssignment: { $ne: true },
                    isPending: { $ne: true },
                    date: {
                        $gte: isFirstBudget ? null : dayjs().year(budget.year).startOf('year').toDate(),
                        $lt: dayjs().year(budget.year).endOf('year').toDate(),
                    },
                };

                let builder = new AggregationBuilder(Transaction);
                const possibleInconsistencies = await builder
                    .match(matchQuery)
                    .group('$description', {
                        categories: { $addToSet: '$category' },
                    })
                    .commit();
                const inconsistencies = possibleInconsistencies.filter((doc) => doc.categories.length > 1);
                if (inconsistencies.length > 0) {
                    throw new Error(
                        `Inconsistencies found. Manual work needs to be done to continue:\n${JSON.stringify(
                            inconsistencies,
                            null,
                            2
                        )}`
                    );
                }

                builder = new AggregationBuilder(Transaction);
                const matchedDescriptions = await builder
                    .match(matchQuery)
                    .group('$category', {
                        descriptions: { $addToSet: { description: '$description' } },
                    })
                    .sort({ _id: 1 })
                    .commit();

                const categories = await Category.find().forUser(budget.userId);
                for (let descriptionGroup of matchedDescriptions) {
                    if (budget.isClosed) {
                        // find category in categoriesSnapshot
                        const category = budget.categoriesSnapshot.find((c) => c.name === descriptionGroup._id);
                        if (category) {
                            category.matchedDescriptions = descriptionGroup.descriptions.map((d) => d.description);
                            budget.markModified('categoriesSnapshot');
                        }
                    } else {
                        const category = categories.find((c) => c.name === descriptionGroup._id);
                        if (category) {
                            category.matchedDescriptions = descriptionGroup.descriptions.map((d) => d.description);
                            await category.save();
                        }
                    }
                }
                await budget.save();
            }
        }
    }
}
module.exports = InitializeSubCategoriesMatchedDescriptions;
