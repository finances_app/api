const AbstractMigrationScript = require('../AbstractMigrationScript');
const { User } = require('../../userCollection');
const { Budget } = require('../../budgetCollection');
const dayjs = require('dayjs');
const { Category } = require('../../categoryCollection');
const { uniq } = require('lodash');

class PatchSubCategoriesMatches extends AbstractMigrationScript {
    static async migrate() {
        for (let user of await User.find()) {
            const previousYearBudget = await Budget.findOne({ userId: user.googleUserId, year: dayjs().year() - 1 });
            if (!previousYearBudget) {
                continue;
            }

            const categories = await Category.find({ type: 1 }).forUser(previousYearBudget.userId);
            for (let category of categories) {
                const lastYearSnapshot = previousYearBudget.categoriesSnapshot.find((c) => c.name === category.name);
                if (lastYearSnapshot && (category.matchedDescriptions || lastYearSnapshot.matchedDescriptions)) {
                    category.matchedDescriptions = uniq([
                        ...(lastYearSnapshot.matchedDescriptions || []),
                        ...(category.matchedDescriptions || []),
                    ]);
                    await category.save();
                }
            }
        }
    }
}
module.exports = PatchSubCategoriesMatches;
