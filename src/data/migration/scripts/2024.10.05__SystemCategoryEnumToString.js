const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Budget } = require('../../budgetCollection');
const { Category } = require('../../categoryCollection');

class SystemCategoryEnumToString extends AbstractMigrationScript {
    static async migrate() {
        const LegacySystemCategory = {
            virement: '3',
            revenu: '4',
            goal: '5',
        };
        for (let category of await Category.find()) {
            switch (category.systemType) {
                case LegacySystemCategory.virement:
                    category.systemType = 'transfer';
                    break;
                case LegacySystemCategory.revenu:
                    category.systemType = 'income';
                    break;
                case LegacySystemCategory.goal:
                    category.systemType = 'goal';
                    break;
                default:
                    if (!category.systemType) {
                        category.systemType = 'expense';
                    }
                    break;
            }
            await category.save();
        }
        const LegacySystemCategoryButForSnapshotData = {
            virement: 3,
            revenu: 4,
            goal: 5,
        };
        for (let budget of await Budget.find({ categoriesSnapshot: { $exists: true, $ne: [] } })) {
            for (let category of budget.categoriesSnapshot) {
                switch (category.systemType) {
                    case LegacySystemCategoryButForSnapshotData.virement:
                        category.systemType = 'transfer';
                        break;
                    case LegacySystemCategoryButForSnapshotData.revenu:
                        category.systemType = 'income';
                        break;
                    case LegacySystemCategoryButForSnapshotData.goal:
                        category.systemType = 'goal';
                        break;
                    default:
                        if (!category.systemType) {
                            category.systemType = 'expense';
                        }
                        break;
                }
            }
            budget.markModified('categoriesSnapshot');
            await budget.save();
        }
    }
}
module.exports = SystemCategoryEnumToString;
