const util = require('util');
const fs = require('fs');
const path = require('path');

const readdir = util.promisify(fs.readdir);
const writeFile = util.promisify(fs.writeFile);

const { User } = require('../userCollection');
const { UserLevel } = require('../../models/enums');
const paths = require('../../../config/paths').default;

class SystemDataManager {
    static async initData() {
        const demoUser = await User.createIfNotExist({
            sub: 'demo_user',
            email: '',
            given_name: 'demo',
            family_name: 'user',
        });
        demoUser.level = UserLevel.readOnly;
        await demoUser.save();
    }

    /**
     * Execute data migration. Everything here must be repeatable !
     * @returns {Promise<boolean>}
     */
    static async migrateData() {
        const successfulMigration = [];
        const migrationState = await this._getMigrationState();
        const migrationClasses = await this._loadMigrationScripts();

        for (let [className, migrationScript] of Object.entries(migrationClasses)) {
            if (!migrationState[className]) {
                console.log(`Executing script '${className}'...`);
                await migrationScript.migrate();
                if (migrationScript.executeOnce) {
                    successfulMigration.push(className);
                }
            }
        }

        await this._saveMigrationState(migrationState, successfulMigration);
    }

    /**
     * Read the state of migration script from the migration state file
     * @returns {Promise<*>}
     * @private
     */
    static async _getMigrationState() {
        let migrationConfigState = {};
        try {
            migrationConfigState = require('../../../migrationState.json');
        } catch (e) {}
        return migrationConfigState;
    }

    /**
     * Save the successful migration to the state file
     * @param migrationConfigState
     * @param successfulMigrations
     * @returns {Promise<void>}
     * @private
     */
    static async _saveMigrationState(migrationConfigState, successfulMigrations) {
        for (let className of successfulMigrations) {
            migrationConfigState[className] = new Date().toISOString();
        }
        await writeFile(paths.MIGRATION_STATE, JSON.stringify(migrationConfigState, null, 4), {
            encoding: 'utf-8',
        });
    }

    /**
     * Find and require all migration scripts
     * @returns {Promise<*>}
     * @private
     */
    static async _loadMigrationScripts() {
        const migrationScriptsBasePath = path.join(__dirname, 'scripts');
        let migrationClasses = {};
        let files = await readdir(migrationScriptsBasePath);
        for (let file of files) {
            migrationClasses[file] = require(path.join(migrationScriptsBasePath, file));
        }
        return migrationClasses;
    }
}
module.exports = SystemDataManager;
