import * as mongodb from 'mongodb';
import { nanoid } from 'nanoid';
import { Stream } from 'stream';
import { Filter } from 'mongodb';
import * as config from 'config';

const MongoClient = mongodb.MongoClient;
const GridFSBucket = mongodb.GridFSBucket;

export type FileDocument = {
    _id: string;
    length: number;
    chunkSize: number;
    uploadDate: string;
    filename: string;
    md5: string;
};

let client: mongodb.MongoClient = null;

async function _getClient() {
    if (!client) {
        const newClient = new MongoClient(config.get('DATA.MONGODB_HOST'));
        await newClient.connect();
        client = newClient;
    }
    return client;
}

async function _getBucket() {
    const mongoClient = await _getClient();
    return new GridFSBucket(mongoClient.db(config.get('INSTANCE')));
}

async function _upload(stream: Stream, filename: string): Promise<string> {
    const bucket = await _getBucket();

    const id = nanoid();
    // @ts-ignore
    const uploadStream = bucket.openUploadStreamWithId(id, filename);

    return new Promise((resolve, reject) => {
        stream
            .pipe(uploadStream)
            .on('error', (err) => reject(err))
            .on('finish', () => resolve(id));
    });
}

export async function save(stream: Stream, filename: string) {
    const fileObjectId = await _upload(stream, filename);
    return { _id: fileObjectId };
}

export async function findOne(query: Filter<FileDocument>) {
    const mongoClient = await _getClient();
    const fileCollection = mongoClient.db(config.get('INSTANCE')).collection<FileDocument>('fs.files');
    return fileCollection.findOne(query);
}

export async function download(fileObjectId: string) {
    const bucket = await _getBucket();
    // @ts-ignore
    return bucket.openDownloadStream(fileObjectId);
}

export async function remove(fileObjectId: string) {
    const bucket = await _getBucket();
    // @ts-ignore
    await bucket.delete(fileObjectId);
}
