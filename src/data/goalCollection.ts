/**
 * Created by hugo on 2023-01-16
 */
import { Model, Schema, Document, model, HydratedDocument, Types } from 'mongoose';
import { GenericQueryHelpers, IGenericSchema, GenericSchema, forUser } from './genericModel';
import { extend } from 'lodash';
import { Goal as GoalDto } from '../models/entities';
import { GoalNecessity } from '../models/enums';

export interface IGoal extends IGenericSchema {
    name: string;
    amount: number;
    description?: string;
    necessity?: number;
    deadline?: Date;
    transactions?: Array<Types.ObjectId>;
    finalAmount?: number;
    completionDate?: Date;
}

export interface IGoalMethods {
    toDto(): GoalDto;
}

interface GoalDocument extends IGoal, Document, IGoalMethods {}

export type HydratedGoal = HydratedDocument<GoalDocument>;

interface GoalModel extends Model<GoalDocument, GenericQueryHelpers<GoalDocument>, IGoalMethods> {}

/**
 * @class Budget
 */
let goalSchema = new Schema<GoalDocument, GoalModel, IGoalMethods, GenericQueryHelpers<GoalDocument>>(
    extend(
        {
            name: { type: String, required: true },
            amount: { type: Number, required: true },
            description: String,
            necessity: { type: Number, default: GoalNecessity.medium },
            deadline: Schema.Types.Date,

            /* Fields filled in once completed */
            transactions: [Types.ObjectId],
            finalAmount: Number,
            completionDate: Schema.Types.Date,
        },
        GenericSchema
    )
);

goalSchema.query.forUser = forUser<GoalDocument>;

goalSchema.method('toDto', function toDto(): GoalDto {
    return {
        id: this._id,
        amount: this.amount,
        name: this.name,
        necessity: this.necessity,
        description: this.description,
        deadline: this.deadline,
        finalAmount: this.finalAmount,
        transactions: this.transactions || [],
        completionDate: this.completionDate,
    };
});

export { goalSchema }
export const Goal = model<GoalDocument, GoalModel>('goal', goalSchema);
