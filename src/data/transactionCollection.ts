/**
 * Created by hugo on 2018-05-13.
 */
import * as _ from 'lodash';
import * as dayjs from 'dayjs';
import * as FileDao from './fileCollection';
import * as Stream from 'stream';
import {
    Schema,
    Types,
    Model,
    model,
    Document,
    HydratedDocument,
    FilterQuery,
    LeanDocument,
    Aggregate,
} from 'mongoose';
import { forUser, GenericQueryHelpers, GenericSchema, IGenericSchema } from './genericModel';
import { AttachmentType } from '../models/enums';
import { TransactionEsEntity, Transaction as TransactionDto } from '../models/entities';
import { SimpleDateInterval } from '../utils/dateUtil';
import AggregationBuilder from '../models/AggregationBuilder';
const { splitDesjardinsDescription } = require('../import/importHelper');

type TransactionAttachment = {
    fileId: string;
    type: AttachmentType;
    filename: string;
};
export interface ITransaction extends IGenericSchema {
    description: string;
    date: Date;
    amount: number;

    account?: string;
    currency?: string;
    conversionRate?: number;

    creationDate: Date;
    statementId?: Types.ObjectId;
    category?: string;
    attachments: Array<TransactionAttachment>;

    ignoreForAutoCategoryAssignment?: boolean;
    missingDetails?: boolean;
    isPending?: boolean;
}

export interface ITransactionMethods {
    toEsEntity(): TransactionEsEntity;
    attachFile(stream: Stream, type: AttachmentType, filename: string): Promise<TransactionAttachment>;
    toDto(): TransactionDto;
}

interface TransactionDocument extends ITransaction, Document, ITransactionMethods {
    descriptionInfo?: {
        type?: string;
        location?: string;
        main?: string;
    };
}
export type LeanTransaction = LeanDocument<TransactionDocument>;
export type HydratedTransaction = HydratedDocument<TransactionDocument>;

interface Transaction
    extends Model<TransactionDocument, GenericQueryHelpers<TransactionDocument>, ITransactionMethods> {
    toDto(transaction: Partial<LeanTransaction & { categoryHierarchy: any }>): TransactionDto;
    getForSubCategoryAssignment(userId: string, transactionId: string): Promise<HydratedTransaction[]>;
    assignCategoryToSimilarTransactions(userId: string, transactionId: string, categoryName: string): Promise<void>;
    getDistinctForSubCategoryAssignment(
        userId: string,
        statementId: string,
        errorsOnly?: boolean
    ): Promise<LeanTransaction[]>;
    getWithCategoryHierarchy(query: FilterQuery<TransactionDocument>): Promise<Aggregate<TransactionWithHierarchy[]>>;
    getBalanceHistory(
        userId: string,
        from: Date,
        to: Date,
        excludedCategories?: string[]
    ): Promise<Array<{ amount: number; _id: { year: number; month: number; day: number } }>>;
    aggregateAmountByCategoryAndPeriod(
        userId: string,
        from: Date,
        to: Date,
        period: SimpleDateInterval
    ): Promise<Aggregate<TransactionAmountByCategory[]>>;
    sumAmountsForSubCategories(userId: string, subCategoryNames: string[], from: Date, to: Date): Promise<number>;
    countByCategories(
        userId: string,
        subCategoryNames: string[],
        from: Date,
        to: Date
    ): Promise<Aggregate<TransactionCountAggregate[]>>;
}

const transactionSchema = new Schema<
    TransactionDocument,
    Transaction,
    ITransactionMethods,
    GenericQueryHelpers<TransactionDocument>
>(
    _.extend(
        {
            // fundamental fields
            description: { type: String, required: true },
            date: { type: Schema.Types.Date, required: true },
            amount: { type: Number, required: true },

            // infered from the context
            account: String,
            currency: String,
            conversionRate: Number,

            // stuff that is tacked on post-import
            creationDate: { type: Schema.Types.Date, default: Date.now },
            statementId: Types.ObjectId,
            category: { type: String, default: '' },
            attachments: [
                {
                    fileId: { type: String, required: true },
                    type: {
                        type: String,
                        required: true,
                        enum: [AttachmentType.file, AttachmentType.eml, AttachmentType.json, AttachmentType.pdf],
                    },
                    filename: String,
                },
            ],
            userId: String,

            // a bunch of flags. bad design ?
            ignoreForAutoCategoryAssignment: Boolean,
            missingDetails: Boolean,
            isPending: Boolean,
        },
        GenericSchema
    )
);

transactionSchema.index({ category: 1, userId: 1 });

// prettier-ignore
transactionSchema.query.forUser = forUser<TransactionDocument>;

transactionSchema.virtual('descriptionInfo').get(function descriptionInfo(this: TransactionDocument) {
    const split = splitDesjardinsDescription(this.description);
    if (this.description?.length === 41 && this.account.includes('CC')) {
        // visa transactions always have exactly 41 characters if un-processed
        return {
            main: this.description.substring(0, 25),
            location: this.description.substring(25),
        };
    } else if (!this.account.includes('CC') && split[0] && split[1]) {
        return {
            type: split[0],
            main: split[1],
        };
    } else {
        return {};
    }
});

transactionSchema.plugin(require('mongoose-lean-virtuals'));

transactionSchema.method('toEsEntity', function toEsEntity(this: TransactionDocument): TransactionEsEntity {
    return {
        id: this.id,
        creationDate: this.creationDate,
        conversionRate: this.conversionRate ? this.conversionRate : 1,
        amount: this.amount,
        currency: this.currency,
        description: this.description?.toLowerCase() || null,
        type: this.descriptionInfo?.type?.toLowerCase() || null,
        location: this.descriptionInfo?.location?.toLowerCase() || null,
        descriptionMain: this.descriptionInfo?.main?.toLowerCase() || null,
        date: this.date,
        category: this.category ? this.category.toLowerCase() : null,
        statement: this.statementId?.toString(),
        userId: this.userId,
        ignoreForAutoCategoryAssignment: this.ignoreForAutoCategoryAssignment,
    };
});

transactionSchema.method('attachFile', async function attachFile(
    stream: Stream,
    type: AttachmentType,
    filename: string
): Promise<TransactionAttachment> {
    let saveRes;
    try {
        saveRes = await FileDao.save(stream, filename);
        const attachment = {
            fileId: saveRes._id,
            type,
            filename,
        };
        this.attachments.push(attachment);
        this.markModified('attachments');
        await this.save();

        return attachment;
    } catch (err) {
        if (saveRes) {
            // failled saving the file, cleanup db
            await FileDao.remove(saveRes._id.toString());
        }
        throw err;
    }
});

transactionSchema.method('toDto', function toDto(): TransactionDto {
    return Transaction.toDto(this);
});

transactionSchema.static('toDto', function toDto(
    transaction: Partial<LeanTransaction & { categoryHierarchy: any }>
): TransactionDto {
    return {
        id: transaction._id?.toString() || transaction.id,
        account: transaction.account,
        currency: transaction.currency,
        amount: transaction.amount,
        attachments: transaction.attachments || [],
        category: transaction.category,
        categoryHierarchy: transaction.categoryHierarchy || [],
        conversionRate: transaction.conversionRate,
        creationDate: transaction.creationDate,
        date: transaction.date,
        statementId: transaction.statementId?.toString(),
        description: transaction.description,
        isPending: transaction.isPending || false,
        descriptionInfo: transaction.descriptionInfo || {},
    };
});

transactionSchema.static('getForSubCategoryAssignment', async function getForSubCategoryAssignment(
    userId: string,
    transactionId: string
): Promise<HydratedTransaction[]> {
    let transaction = await this.findOne({ _id: transactionId }).forUser<LeanTransaction>(userId).lean();
    if (!transaction) {
        throw new Error(`No transaction with id ${transactionId}`);
    }

    return this.find({
        description: transaction.description,
        statementId: transaction.statementId,
    }).sort('date');
});

transactionSchema.static('assignCategoryToSimilarTransactions', async function assignCategoryToSimilarTransactions(
    this: Transaction,
    userId: string,
    transactionId: string,
    categoryName: string
): Promise<void> {
    let transaction = await this.findOne({ _id: transactionId }).forUser<LeanTransaction>(userId).lean();
    const date = dayjs(transaction.date);
    await this.updateMany(
        {
            userId,
            description: transaction.description,
            ignoreForAutoCategoryAssignment: { $ne: true },
            date: {
                $gte: date.startOf('year').toDate(),
                $lt: date.endOf('year').toDate(),
            },
        },
        {
            $set: { category: categoryName },
        }
    );
});

transactionSchema.static('getDistinctForSubCategoryAssignment', async function getDistinctForSubCategoryAssignment(
    userId: string,
    statementId: string,
    errorsOnly?: boolean
): Promise<LeanTransaction[]> {
    let transactions = await this.find({
        userId: userId,
        statementId: statementId,
    }).lean();

    return transactions
        .reduce((distinctTransactions, transaction) => {
            if (!distinctTransactions.some((t) => t.description === transaction.description)) {
                distinctTransactions.push(transaction);
            }
            return distinctTransactions;
        }, [])
        .filter((_transaction) => !errorsOnly || !_transaction.category)
        .sort((a, b) => {
            let aString = a.description.toLowerCase();
            let bString = b.description.toLowerCase();
            if (aString < bString) return -1;
            if (aString > bString) return 1;
            return 0;
        });
});

type TransactionWithHierarchy = LeanTransaction & {
    category_hierarchy: Array<{
        depth: number;
        name: string;
    }>;
};
transactionSchema.static('getWithCategoryHierarchy', async function getWithCategoryHierarchy(
    query: FilterQuery<TransactionDocument>
): Promise<Aggregate<TransactionWithHierarchy[]>> {
    return this.aggregate([
        {
            $match: query,
        },
        {
            $graphLookup: {
                from: 'categories',
                startWith: '$category',
                connectFromField: 'parent',
                connectToField: 'name',
                as: 'category_hierarchy',
                depthField: 'depth',
            },
        },
    ]);
});

transactionSchema.static('getBalanceHistory', async function getBalanceHistory(
    userId: string,
    from: Date,
    to: Date,
    excludedCategories?: string[]
): Promise<Array<{ amount: number; _id: { year: number; month: number; day: number } }>> {
    const builder = new AggregationBuilder(this);
    return builder
        .match({
            userId: userId,
            date: { $gte: from, $lte: to },
            category: { $nin: excludedCategories },
        })
        .group(
            {
                year: { $year: '$date' },
                month: { $month: '$date' },
                day: { $dayOfMonth: '$date' },
            },
            { amount: { $sum: '$amount' } }
        )
        .sort({ '_id.year': 1, '_id.month': 1, '_id.day': 1 })
        .commit();
});

type TransactionAmountByCategory = {
    _id: {
        category: string;
        year: number;
        t?: number;
        M?: number;
    };
    amount: number;
};
transactionSchema.static('aggregateAmountByCategoryAndPeriod', async function aggregateAmountByCategoryAndPeriod(
    userId: string,
    from: Date,
    to: Date,
    period: SimpleDateInterval
): Promise<Aggregate<TransactionAmountByCategory[]>> {
    const builder = new AggregationBuilder(this);

    // default value is for year grouping.
    let groupByExpression: any = {
        category: '$category',
    };
    let sortExpression: Record<string, 1 | -1> = {
        '_id.category': 1,
    };

    if (period) {
        groupByExpression['year'] = { $year: '$date' };

        if (period === 't') {
            groupByExpression[period] = {
                $floor: builder.divide(builder.subtract({ $month: '$date' }, 1), 4),
            };
        } else if (period === 'M') {
            groupByExpression[period] = builder.subtract({ $month: '$date' }, 1);
        }

        sortExpression['_id.year'] = 1;
        if (period !== 'y') {
            sortExpression[`_id.${period}`] = 1;
        }
    }

    return builder
        .match({
            userId,
            date: { $gte: from, $lt: to },
            category: {
                $exists: true,
                $nin: [null, ''],
            },
        })
        .group(groupByExpression, { amount: { $sum: '$amount' } })
        .sort(sortExpression)
        .commit();
});

transactionSchema.static('sumAmountsForSubCategories', async function sumAmountsForSubCategories(
    userId: string,
    subCategoryNames: string[],
    from: Date,
    to: Date
): Promise<number> {
    const builder = new AggregationBuilder(this);
    const res = await builder
        .match({
            userId,
            category: { $in: subCategoryNames },
            date: {
                $gte: from,
                $lte: to,
            },
        })
        .group(null, { sum: { $sum: '$amount' } })
        .commit();
    return (res[0] && res[0].sum) || 0;
});

type TransactionCountAggregate = {
    _id: string;
    count: number;
};
transactionSchema.static('countByCategories', async function countByCategories(
    userId: string,
    subCategoryNames: string[],
    from: Date,
    to: Date
): Promise<Aggregate<TransactionCountAggregate[]>> {
    const builder = new AggregationBuilder(this);
    return builder
        .match({
            userId,
            date: {
                $gte: from,
                $lte: to,
            },
            category: { $in: subCategoryNames },
        })
        .group('$category', { count: { $sum: 1 } })
        .commit();
});

export { transactionSchema };
export const Transaction = model<TransactionDocument, Transaction>('transaction', transactionSchema);
