import * as express from 'express';
import * as bodyParser from 'body-parser';
import { handle404 } from './utils/errorHandler';
import * as Sentry from '@sentry/node';
import * as fs from 'fs';
import * as https from 'https';
import { initApiLogger, initSentry } from './middlewares/telemetry';
import getApiRouter from './_resources_/apiRoutes';
import getPublicRouter from './_resources_/publicRoutes';

const isProd = process.env.NODE_ENV === 'PROD';
const isDev = process.env.NODE_ENV === 'DEV';
const isCI = process.env.NODE_ENV === 'CI';

export async function startApiWorker() {
    const httpPort = isDev ? 9091 : 80;
    const httpsPort = isDev ? 9092 : 443;

    const app = setupExpressApp();

    if (isDev || isCI) {
        const _port = process.env.CI_PORT || httpPort;
        app.listen(_port);
        console.log(`http server started on port ${_port}`);
    } else {
        let credentials = {
            key: fs.readFileSync(process.env.HTTPS_PRIVK_PATH, 'utf8'),
            cert: fs.readFileSync(process.env.HTTPS_CERT_PATH, 'utf8'),
        };
        let httpsServer = https.createServer(credentials, app);
        httpsServer.listen(httpsPort);
        console.log(`https server started on port ${httpsPort}`);
    }
}

function setupExpressApp() {
    let app = express();

    if (isProd) {
        initSentry({
            app,
            sampleRate: isProd ? 0.2 : 1,
            environment: isProd ? 'production' : 'development',
        });
        app.use(Sentry.Handlers.requestHandler());
        app.use(Sentry.Handlers.tracingHandler());
    } else {
        initApiLogger(app);
    }

    app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
    app.use(bodyParser.json({ limit: '5mb' }));

    app.use(getPublicRouter());

    app.use(require('compression')());
    app.use(getApiRouter());

    if (isProd) {
        app.use(Sentry.Handlers.errorHandler());
    }
    app.use((req, res) => handle404(req, res, null));

    return app;
}
