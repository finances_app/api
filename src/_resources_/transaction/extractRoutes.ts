import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler } from '../../utils/queryUtil';
import * as _ from 'lodash';
import * as dayjs from 'dayjs';
import { processDiff, TransactionDiff } from '../../utils/transactionUtils';
import { toInt } from '../../utils/queryUtil';
import { ClientError } from '../../utils/errors';
import { getStream } from '../../utils/multipartUtil';
import { findExistingTransactions } from './transactionController';
import { Transaction } from '../../data/transactionCollection';
import { EssentialTransactionData, Transaction as TransactionDto } from '../../models/entities';
import { iterateTransactionsWithMatchingCategory } from '../category/categoryController';
const transactionProcessor = require('../../import/csv_processor/transactionProcessor');
const searchablePdfProcessor = require('../../import/pdf_processor/searchablePdfProcessor');

export default function getExtractRouter() {
    const router = makeAuthenticatedRouter();

    router.route('/diff/:statement_type').post(makeRequestHandler(extractStatementAndCompare));

    return router;
}

// POST /api/extract/diff:statement_type
async function extractStatementAndCompare(req: AuthenticatedRequest) {
    let params = req.params;
    let fileType = toInt(params.statement_type);

    const { stream, filename } = await getStream(req);
    if (!stream) {
        throw new ClientError(`Aucun fichier n'a été reçu.`);
    }

    let newTransactions: EssentialTransactionData[] = [];

    let fileExtension = filename.split('.').pop();
    switch (fileExtension.toLowerCase()) {
        case 'csv':
            newTransactions = await transactionProcessor.extract(fileType, stream);
            break;
        case 'pdf':
            newTransactions = await searchablePdfProcessor.extract(fileType, stream);
            break;
        default:
            throw new ClientError(`Type de fichier non supporté (${fileExtension}).`);
    }

    await iterateTransactionsWithMatchingCategory(req.userId, newTransactions, (transaction, category) => {
        transaction.category = category.name;
    });
    newTransactions = _.sortBy(newTransactions, (_t) => new Date(_t.date).getTime());

    let existingTransactions = await findExistingTransactions(newTransactions, req.userId);
    existingTransactions = _.sortBy(existingTransactions, (_t) => new Date(_t.date).getTime());

    const diffs = processDiff(
        newTransactions.map((_t) => Transaction.toDto(_t)),
        existingTransactions.map((_t) => Transaction.toDto(_t))
    );
    return {
        items: newTransactions,
        diffs: filterRemovedDiffsOnMonthStartAndEnd(diffs, newTransactions),
    };
}

function filterRemovedDiffsOnMonthStartAndEnd(
    diffs: TransactionDiff<TransactionDto>[],
    newTransactions: EssentialTransactionData[]
) {
    // remove "removed" diffs on the first and last day, those will be ignored anyway at the import.
    return diffs.filter((diff) => {
        if (diff.removed) {
            diff.removed = diff.removed.filter(
                (_t) =>
                    !dayjs(_.first(newTransactions).date).isSame(dayjs(_t.date), 'day') &&
                    !dayjs(_.last(newTransactions).date).isSame(dayjs(_t.date), 'day')
            );
            if (diff.removed.length === 0) {
                return false;
            }
        }
        return true;
    });
}
