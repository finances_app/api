import { CategoryType, StatementFileType } from '../../models/enums';
import * as dayjs from 'dayjs';
import { FilterQuery } from 'mongoose';
import { Category } from '../../data/categoryCollection';
import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import { Budget } from '../../data/budgetCollection';
import { PlannedTransaction } from '../../data/plannedTransactionCollection';
import { ClientError } from '../../utils/errors';
import { EssentialTransactionData } from '../../models/entities';
import { getCategories, updatePlannedTransactions } from '../budget/budgetController';
import { BudgetModelBuilder } from '../../models/BudgetModelBuilder';
import { BudgetEntryDocument, VirtualPlannedTransaction } from '../../data/budgetEntryCollection';
import { nanoid } from 'nanoid';
import { pairCoordinatesByNearestNeighbour } from '../../utils/geometryUtil';
import { ITransaction, LeanTransaction, Transaction } from '../../data/transactionCollection';
import { areTransactionSameIsh, findUniqAccounts, iterateDiffs } from '../../utils/transactionUtils';
import { HydratedUser } from '../../data/userCollection';
import { findOrCreateStatement } from '../statement/statementController';
import { findAndCreateNewCategories, iterateTransactionsWithMatchingCategory } from '../category/categoryController';
import { createCacheExpirationEvents } from '../../caching/clientCache';
import { deleteTransactionsById, indexTransactions } from '../../data/elasticsearch/elasticsearch';

type WithComparingId<T> = T & { __comparingId?: string };

const allowedFields = [
    'description',
    'date',
    'amount',
    'currency',
    'conversionRate',
    'creationDate',
    'account',
    'category',
];
export async function searchTransactions({
    userId,
    size,
    skip,
    filter,
    sort,
    preSearchIds,
}: {
    userId: string;
    size?: number;
    skip?: number;
    filter?: Record<string, any>;
    sort?: Record<string, 1 | -1>;
    preSearchIds?: string[];
}): Promise<Array<LeanTransaction>> {
    let query: FilterQuery<any> = {
        userId,
        ...(await applyFilters(filter, userId)),
    };
    if (preSearchIds) {
        query._id = {
            $in: preSearchIds.map((id) => new mongoose.Types.ObjectId(id)),
        };
    }

    const sortQuery: Record<string, 1 | -1> = {};
    if (!_.isEmpty(sort)) {
        for (const [field, direction] of Object.entries(sort)) {
            if (allowedFields.includes(field)) {
                sortQuery[field] = direction;
            }
        }
    }
    sortQuery['_id'] = 1;

    const queryBuilder = Transaction.find(query).sort(sortQuery);
    if (!preSearchIds && size) {
        queryBuilder.skip(skip || 0).limit(size);
    }
    return queryBuilder.lean({ virtuals: true });
}

async function applyFilters(filter: Record<string, any>, userId: string) {
    const query: FilterQuery<any> = {};
    if (_.isEmpty(filter)) {
        return query;
    }

    const applyFilterMapping: Record<string, (field: string, value: any) => Promise<void>> = {
        statementId: async (field, value) => {
            query[field] = new mongoose.Types.ObjectId(value);
        },

        categoryId: async (_field, value) => {
            const budget = await Budget.getClosest(
                userId,
                'date_from' in filter ? dayjs(filter['date_from']).year() : dayjs().year()
            );
            const categories = await getCategories(budget);
            const category = categories.find((c) => c._id.equals(value));
            if (category) {
                const baseCategories = await Category.findChildBaseCategories(userId, category.name, categories);
                query['category'] = {
                    $in: baseCategories.map((c) => c.name),
                };
            } else {
                // The category doesn't exist, `$in: []` essentially means that the filter will never match anything.
                query['category'] = { $in: [] };
            }
        },

        non_transfer: async () => {
            query['category'] = {
                $nin: await Category.getTransferCategoryNames(userId),
            };
        },

        no_category: async () => {
            query['category'] = { $in: [null, ''] };
            query['isPending'] = { $ne: true };
        },

        date_from: async (_field, value) => {
            query['date'] = {
                ...query['date'],
                $gte: new Date(value),
            };
        },

        date_to: async (_field, value) => {
            query['date'] = {
                ...query['date'],
                $lt: new Date(value),
            };
        },

        date: async (field, value) => {
            query[field] = new Date(value);
        },
    };

    for (const [field, value] of Object.entries(filter)) {
        if (!!applyFilterMapping[field]) {
            await applyFilterMapping[field](field, value);
        } else if (allowedFields.includes(field)) {
            query[field] = _.isArray(value) ? { $in: value } : value;
        }
    }
    return query;
}

export async function appendOptionalFields(userId: string, fields: string[], transactions: LeanTransaction[]) {
    const additionalFields = new Map();
    for (let field of fields) {
        if (field === 'categoryHierarchy') {
            // group transactions by year and get the correct categories for the year's budget
            const groups: Record<string, any[]> = _.groupBy(transactions, (_t: any) => _t.date.getFullYear());
            for (let [year, transactionsGroup] of Object.entries(groups)) {
                const budgetModel = await BudgetModelBuilder.create({
                    userId,
                    modelYear: parseInt(year),
                }).build();
                const hierarchyMap = budgetModel.getCategoryHierarchy((categoryInfo) => ({
                    _id: categoryInfo.category._id,
                    name: categoryInfo.category.name,
                    hue: categoryInfo.category.hue,
                    systemType: categoryInfo.category.systemType,
                    type: categoryInfo.category.type,
                }));

                for (let transaction of transactionsGroup) {
                    const id = transaction._id.toString();
                    let values = {
                        ...additionalFields.get(id),
                        [field]: hierarchyMap.get(transaction.category),
                    };
                    additionalFields.set(id, values);
                }
            }
        } else {
            throw new ClientError(`Le champs '${field}' n'est pas valide.`);
        }
    }

    return transactions.map((_transaction) =>
        Transaction.toDto({
            ..._transaction,
            ...additionalFields.get(_transaction._id.toString()),
        })
    );
}

export function createPlannedTransactionMatcher(
    transactions?: WithComparingId<LeanTransaction>[],
    plannedTransactions?: WithComparingId<VirtualPlannedTransaction>[],
    budgetEntry?: BudgetEntryDocument
) {
    const identifiedTransactions =
        transactions?.map((t) => {
            t.__comparingId = t._id?.toString() || nanoid();
            return t;
        }) || [];
    const identifiedPlannedTransactions =
        plannedTransactions?.map((t) => {
            t.__comparingId = nanoid();
            return t;
        }) || [];

    return {
        pair: () => pairPlannedTransactions(identifiedTransactions, identifiedPlannedTransactions, budgetEntry),
    };
}

function pairPlannedTransactions(
    transactions: WithComparingId<LeanTransaction>[],
    plannedTransactions: WithComparingId<VirtualPlannedTransaction>[],
    budgetEntry?: BudgetEntryDocument
) {
    const maximumSloppinessInDays = budgetEntry?.getMaximumSloppinessInDays() || 0;

    // zip planned transactions with the observed ones
    // Timestamps is on a scale of milliseconds. The smallest useful resolution is a day (86400000 ms)
    // One day has the same distance as 1000$
    const timestampFactor = 1 / 86400;
    const amountFactor = 1;
    const formatValue = (value: number, factor: number) => Math.round(value * factor);
    const coordinateMapping = pairCoordinatesByNearestNeighbour(
        plannedTransactions
            .map((t) => ({
                id: t.__comparingId,
                x: formatValue(new Date(t.date).getTime(), timestampFactor),
                y: formatValue(t.amount, amountFactor),
            }))
            .reverse(),
        transactions
            .map((t) => ({
                id: t.__comparingId,
                x: formatValue(new Date(t.date).getTime(), timestampFactor),
                y: formatValue(t.amount, amountFactor),
            }))
            .reverse(),
        { distanceThreshold: maximumSloppinessInDays * 1000 }
    );

    const plannedTransactionMapping = new Map<string, WithComparingId<VirtualPlannedTransaction> | null>();
    for (const [plannedId, transactionId] of coordinateMapping) {
        if (!transactionId) {
            continue;
        }
        plannedTransactionMapping.set(
            transactionId,
            plannedTransactions.find((t) => t.__comparingId === plannedId)
        );
    }
    return plannedTransactionMapping;
}

export async function validateTransactions<T extends EssentialTransactionData>(userId: string, transactions: T[]) {
    // check that no group category was assigned
    const categoryNames = _.chain(transactions)
        .map((_t) => _t.category)
        .uniq()
        .filter(Boolean)
        .value();
    const categoryCount = await Category.countDocuments({
        userId,
        type: CategoryType.group,
        name: {
            $in: categoryNames,
        },
    });
    if (categoryCount > 0) {
        return false;
    }

    return transactions.every((_transactions) => {
        if (typeof _transactions.description !== 'string' || !_transactions.description) {
            return false;
        } else if (typeof _transactions.amount !== 'number') {
            return false;
        } else if (typeof _transactions.account !== 'string' || !_transactions.account) {
            return false;
        } else if (typeof _transactions.currency !== 'string' || !_transactions.currency) {
            return false;
        } else if (!_transactions.date || _.isNaN(new Date(_transactions.date))) {
            return false;
        }
        return true;
    });
}

export async function removeTransactionsById(transactionIds: string[]) {
    await Transaction.remove({
        _id: { $in: transactionIds },
    });
    await deleteTransactionsById(transactionIds);
    await unReferencePlannedTransactions(transactionIds);
}

export async function unReferencePlannedTransactions(transactionIds: string[]) {
    const items = await PlannedTransaction.find({ matchedTransactions: { $in: transactionIds } });
    for (const item of items) {
        item.matchedTransactions = item.matchedTransactions?.filter((t) => !transactionIds.includes(t.toString()));
        await item.save();
    }
}

export function findExistingTransactions<T extends EssentialTransactionData>(
    transactions: T[],
    userId: string,
    // TODO: An other dirty hack related to the visa date offset glitch
    offsetDays = 0
) {
    const accounts = findUniqAccounts(transactions);

    const transactionDates = _.chain(transactions)
        .map((_t) => new Date(_t.date))
        .sortBy((_date) => _date.getTime())
        .value();
    return Transaction.find({
        account: {
            $in: accounts,
        },
        date: {
            $gte: dayjs(_.first(transactionDates)).subtract(offsetDays, 'day').toDate(),
            $lte: dayjs(_.last(transactionDates)).add(offsetDays, 'day').toDate(),
        },
    })
        .forUser(userId)
        .lean()
        .exec();
}

export async function replaceTransactions(user: HydratedUser, transactions: EssentialTransactionData[]) {
    // FIXME: Remove this next line as soon as possible :)
    const _transactions = await patchVisaDateOffsetBug(user.googleUserId, transactions);
    const existingTransactions = await findExistingTransactions(_transactions, user.googleUserId);

    let newTransactions: EssentialTransactionData[] = [];
    iterateDiffs(_transactions, existingTransactions, (added, removed) => {
        if (added && !removed) {
            newTransactions.push(added);
        }
    });

    if (_.isEmpty(newTransactions)) {
        return [];
    }

    let hydratedTransactions: ITransaction[] = [];
    for (let [month, transactionGroup] of Object.entries(_.groupBy(newTransactions, (_t) => dayjs(_t.date).month()))) {
        let firstOfMonth = dayjs(transactionGroup[0].date).month(parseInt(month)).startOf(`month`).toDate();
        let statement = await findOrCreateStatement(user.googleUserId, firstOfMonth, StatementFileType.webScraper);
        hydratedTransactions.push(
            ...transactionGroup.map((t) => ({
                ...t,
                statementId: statement._id,
                userId: user.googleUserId,
                attachments: [],
                creationDate: undefined,
            }))
        );
    }
    await iterateTransactionsWithMatchingCategory(user.googleUserId, hydratedTransactions, (transaction, category) => {
        transaction.category = category.name;
    });

    const insertedTransactions = await Transaction.insertMany(hydratedTransactions);

    await updatePlannedTransactions(user.googleUserId, insertedTransactions);
    await findAndCreateNewCategories(user.googleUserId, insertedTransactions);

    await createCacheExpirationEvents(user, insertedTransactions);
    await indexTransactions(insertedTransactions);

    return insertedTransactions;
}

/**
 * FIXME whenever visa transactions stop being randomly offset by a day from time to time... REEEEEEEEEEEEEE !!<br/>
 * Attempts to correct for the 24h offset with Visa transactions by checking if duplicate transactions exist with a 1
 * day offset.
 * @deprecated This function is a patch, remove when Desjardins fixes their stuff.
 */
async function patchVisaDateOffsetBug(userId: string, transactions: EssentialTransactionData[]) {
    const existingTransactions = await findExistingTransactions(transactions, userId, 1);

    const patchedTransactions = [];
    for (let transaction of transactions) {
        // if the existing transaction was imported with an offset date and is seen again without the offset, we need to
        // update the existing one with the new date.
        const _transaction = {
            ...transaction,
            date: new Date(transaction.date),
        };
        const existingTransactionWithOffset = existingTransactions.find((existing) => {
            const existingWithOffset = {
                ...existing,
                date: dayjs(existing.date).add(1, 'day').toDate(),
            };
            const _existing = {
                ...existing,
                date: new Date(existing.date),
            };
            // FIXME: Naive implementation. It is more complicated than that if there are multiple transactions with the
            //        same amount, multiple days in a row, or on the same day.
            const hasMatchWithoutOffset = areTransactionSameIsh(_existing, _transaction);
            const hasMatchWithOffset = areTransactionSameIsh(existingWithOffset, _transaction);
            return hasMatchWithOffset && !hasMatchWithoutOffset;
        });
        if (existingTransactionWithOffset) {
            // Not updating 'existingTransactionWithOffset' intentionally
            await Transaction.updateOne(
                {
                    _id: existingTransactionWithOffset._id,
                },
                {
                    $set: {
                        date: new Date(transaction.date),
                    },
                }
            );
        }

        // try with +24h offset
        const isVisaAccount = transaction.account.includes('CC');
        const transactionWithOffsetDate = {
            ...transaction,
            date: dayjs(transaction.date).add(1, 'day').toDate(),
        };
        const existsWithOffset =
            isVisaAccount &&
            existingTransactions.some((existing) => {
                const _existing = {
                    ...existing,
                    date: new Date(existing.date),
                };
                const hasMatchWithOffset = areTransactionSameIsh(_existing, transactionWithOffsetDate);
                const hasMatchWithoutOffset = areTransactionSameIsh(_existing, _transaction);
                return hasMatchWithOffset && !hasMatchWithoutOffset;
            });
        patchedTransactions.push(existsWithOffset ? transactionWithOffsetDate : transaction);
    }
    return patchedTransactions;
}
