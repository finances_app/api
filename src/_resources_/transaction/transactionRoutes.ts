/**
 * Created by hugo on 2018-05-18
 */
import {
    AuthenticatedRequest,
    makeAuthenticatedRouter,
    makeRequestHandler,
    toBoolean,
    toInt,
    toObject,
} from '../../utils/queryUtil';
import { getAttachmentTransactionRouter } from '../system/attachmentRoutes';
import { defaultContext } from '../../utils/context';
import {
    appendOptionalFields,
    replaceTransactions,
    searchTransactions,
    unReferencePlannedTransactions,
    validateTransactions,
} from './transactionController';
import { Transaction } from '../../data/transactionCollection';
import { ClientError } from '../../utils/errors';
import { Category } from '../../data/categoryCollection';
import { iterateTransactionsWithMatchingCategory } from '../category/categoryController';
import { updatePlannedTransactions } from '../budget/budgetController';
import { Budget, LeanBudget } from '../../data/budgetCollection';
import { CategoryType } from '../../models/enums';
import { updateApprovedStatus } from '../statement/statementController';
import * as dayjs from 'dayjs';
import * as _ from 'lodash';
import {
    createCategoryGuesstimator,
    deleteEsTransaction,
    esSearch,
    upsertEsTransactions,
} from '../../data/elasticsearch/elasticsearch';
import { Request, Response } from 'express';
import * as FileDao from '../../data/fileCollection';

// prettier-ignore
export default function getTransactionRouter() {
    const router = makeAuthenticatedRouter();

    router.use(getAttachmentTransactionRouter());

    router.route('/pending')
        .put(makeRequestHandler(replacePendingTransactions));
    router.route('/assign_category/:transaction_id')
        .all(defaultContext)
        .put(makeRequestHandler(assignCategory));
    router.route('/goal/:goal_id')
        .all(defaultContext)
        .get(makeRequestHandler(listGoalRelatedTransactions));
    router
        .route('/:transaction_id')
        .all(defaultContext)
        .get(makeRequestHandler(getTransaction))
        .put(makeRequestHandler(updateTransaction))
        .delete(makeRequestHandler(deleteTransaction));
    router.route('/')
        .get(makeRequestHandler(getMany))
        .put(makeRequestHandler(replaceMany));

    return router;
}

/**
 * List transactions for the user, with optional filters, sorting and paging.
 */
async function getMany(req: AuthenticatedRequest) {
    /**
     * @property {string} size
     * @property {string} skip
     * @property {string} filter
     * @property {string} sort
     */
    const query = req.query;

    const size = toInt(query.size?.toString() || '50');
    const skip = toInt(query.skip?.toString() || '0');
    const filter = toObject(query.filter?.toString()) || {};
    const sort = toObject(query.sort?.toString()) || {};
    const fields = toObject(query.fields?.toString()) || [];

    let transactionIds = null;
    if (query.q) {
        let from = filter?.date_from && new Date(filter?.date_from);
        let to = filter?.date_to && new Date(filter?.date_to);
        transactionIds = await esSearch(req.userId, query.q?.toString(), size, skip, from, to);
    }

    const transactions = await searchTransactions({
        userId: req.userId,
        size,
        skip,
        filter,
        sort,
        preSearchIds: transactionIds,
    });
    return {
        items: await appendOptionalFields(req.userId, fields, transactions),
    };
}

async function listGoalRelatedTransactions(req: AuthenticatedRequest) {
    const goal = req.context.goal_id;
    const transactions = await Transaction.find({ _id: { $in: goal.transactions } })
        .forUser(req.userId)
        .lean();

    return {
        items: await appendOptionalFields(req.userId, ['categoryHierarchy'], transactions),
    };
}

async function replacePendingTransactions(req: AuthenticatedRequest) {
    let transactions = req.body;

    if (!(await validateTransactions(req.userId, transactions))) {
        throw new ClientError(`Transaction validation error.`);
    }

    await Transaction.deleteMany({
        userId: req.userId,
        isPending: true,
    });

    const categoryGuesser = await createCategoryGuesstimator(req.userId, transactions);
    const categories = await Category.find().forUser(req.userId).lean();

    await iterateTransactionsWithMatchingCategory(req.userId, transactions, (transaction, category) => {
        transaction.category = category.name;
    });
    for (let transaction of transactions) {
        transaction.userId = req.userId;
        transaction.isPending = true;
        let categoryMatch = categoryGuesser.find(transaction.descriptionInfo || transaction.description);
        if (categoryMatch && !transaction.category) {
            let category = categories.find((category) => category.name.toLowerCase() === categoryMatch.category);
            transaction.category = category?.name;
        }
    }

    const newTransactions = await Transaction.insertMany(transactions);
    await updatePlannedTransactions(req.userId, newTransactions);

    if (!_.isEmpty(newTransactions)) {
        await req.user.addCacheExpirationEvents(['TRANSACTIONS_IMPORTED']);
    }
}

/**
 * Insert new transactions only, ignoring the ones that already exist.
 * Assign a category if none is specified
 */
async function replaceMany(req: AuthenticatedRequest) {
    let transactions = req.body;

    if (!(await validateTransactions(req.userId, transactions))) {
        throw new ClientError(`Transaction validation error.`);
    }

    const updatedTransactions = await replaceTransactions(req.user, transactions);
    return { items: updatedTransactions.map((t) => t.toDto()) };
}

async function getTransaction(req: AuthenticatedRequest) {
    let query = req.query;
    const transactionId = req.params.transaction_id;
    const transaction = req.context.transaction_id;

    if (transaction && !!query.forSubCategoryAssignment) {
        let transactions = await Transaction.getForSubCategoryAssignment(req.userId, transactionId);

        // get previous and next transactions id
        let distinctTransactions = await Transaction.getDistinctForSubCategoryAssignment(
            req.userId,
            transactions[0].statementId.toString(),
            true
        );
        let distinctTransactionIds = distinctTransactions.map((t) => t._id.toString());
        let currentTransactionIndex = distinctTransactionIds.indexOf(transactionId);
        let previousTransactionId = null;
        let nextTransactionId = null;

        if (currentTransactionIndex > 0) {
            previousTransactionId = distinctTransactionIds[currentTransactionIndex - 1];
        }
        if (currentTransactionIndex < distinctTransactionIds.length - 1) {
            nextTransactionId = distinctTransactionIds[currentTransactionIndex + 1];
        }

        return {
            transactions: transactions,
            previousTransactionId: previousTransactionId,
            nextTransactionId: nextTransactionId,
        };
    } else if (transaction) {
        return { transaction };
    } else {
        throw new ClientError(`Transaction cannot be queried for field(s): ${Object.keys(query)}`, 404);
    }
}

/**
 Used in old import view
 * @deprecated
 */
async function assignCategory(req: AuthenticatedRequest) {
    const transactionId = req.params.transaction_id;
    const transaction = req.context.transaction_id;

    let body = req.body;
    if (!body.category_name) {
        throw new ClientError(`category_name is required.`);
    }

    const relatedBudgets = await Budget.find({
        year: {
            $lte: transaction.date.getFullYear(),
        },
    })
        .forUser<LeanBudget[]>(req.userId)
        .limit(1)
        .sort('-year')
        .select('categoriesSnapshot')
        .lean();
    const relatedBudget = relatedBudgets[0];
    // if the transaction can be scoped to a closed budget of previous years, prevent editing the transaction
    if (relatedBudget && !_.isEmpty(relatedBudget.categoriesSnapshot)) {
        throw new ClientError(
            `La transaction est lié à un budget fermé. Pour modifier la transaction, ` +
                `utilisez la fonctionnalité 'Time Travel' vers le budget correspondant.`
        );
    }

    let transactionsToUpdate = await Transaction.getForSubCategoryAssignment(req.userId, transactionId);

    await Category.createIfNotExistsOrUpdate(req.userId, CategoryType.base, body.category_name, body.system_type, [
        transaction.description,
    ]);
    await Transaction.updateMany(
        { _id: { $in: transactionsToUpdate.map((_t) => _t._id) } },
        { $set: { category: body.category_name } }
    );

    // check if the statement was created by the web scrapper
    if (transactionsToUpdate.length > 0) {
        await updateApprovedStatus({
            userId: req.userId,
            statementId: transactionsToUpdate[0].statementId.toString(),
        });
    }

    await upsertEsTransactions(
        transactionsToUpdate.map((t) => ({
            ...t.toEsEntity(),
            category: body.category_name,
        }))
    );
}

async function updateTransaction(req: AuthenticatedRequest) {
    let updatedTransaction = req.body.transaction;
    const transaction = req.context.transaction_id;

    transaction.account = updatedTransaction.account;
    transaction.amount = updatedTransaction.amount;
    transaction.currency = updatedTransaction.currency;
    transaction.date = updatedTransaction.date;
    transaction.description = updatedTransaction.description;
    transaction.statementId = updatedTransaction.statementId;

    if (transaction.category !== updatedTransaction.category) {
        const relatedBudgets = await Budget.find({
            userId: req.userId,
            year: {
                $lte: dayjs(updatedTransaction.date).year(),
            },
        })
            .limit(1)
            .sort('-year')
            .select('categoriesSnapshot year')
            .lean();
        const relatedBudget = relatedBudgets[0];
        // if the transaction can be scoped to a closed budget of previous years, prevent editing the transaction
        if (relatedBudget && !_.isEmpty(relatedBudget.categoriesSnapshot) && relatedBudget.year < dayjs().year()) {
            throw new ClientError(
                `La transaction est lié à un budget fermé. Pour modifier la transaction, ` +
                    `utilisez la fonctionnalité 'Time Travel' vers le budget correspondant.`
            );
        }

        await Category.createIfNotExistsOrUpdate(
            req.userId,
            CategoryType.base,
            updatedTransaction.category,
            req.body.systemType,
            [transaction.description]
        );

        const applyCategoryToAll = toBoolean(req.query.applyCategoryToAll?.toString()) ?? false;
        transaction.ignoreForAutoCategoryAssignment = !applyCategoryToAll;
        transaction.category = updatedTransaction.category;

        if (applyCategoryToAll) {
            await Transaction.assignCategoryToSimilarTransactions(
                req.userId,
                req.params.transaction_id,
                updatedTransaction.category
            );
        }

        if (updatedTransaction.statementId) {
            await updateApprovedStatus({
                userId: req.userId,
                statementId: updatedTransaction.statementId.toString(),
            });
        }
    }

    await transaction.save();
    await upsertEsTransactions([transaction.toEsEntity()]);
    await updatePlannedTransactions(req.userId, [transaction]);
}

async function deleteTransaction(req: AuthenticatedRequest) {
    const transaction = req.context.transaction_id;
    await transaction.remove();
    try {
        await deleteEsTransaction(req.params.transaction_id);
    } catch (err) {
        if (!transaction.isPending) {
            throw err;
        }
    }
    await unReferencePlannedTransactions([req.params.transaction_id]);
}

export async function getGasPrice(req: Request, res: Response) {
    const transaction = await Transaction.find({
        'attachments.filename': 'data.json',
    })
        .sort({ date: -1 })
        .limit(1)
        .lean();
    if (transaction.length) {
        const attachment = transaction[0].attachments.find((_a) => _a.filename === 'data.json');
        const stream = await FileDao.download(attachment.fileId);
        // convert GrisfsBucketReadStream to string
        let data = '';
        for await (const chunk of stream) {
            data += chunk;
        }
        const jsonData = JSON.parse(data);

        const gasPrice = jsonData.estimatedGasPrice;
        if (gasPrice) {
            res.send(`⛽ About ${gasPrice} ⛽`);
            return;
        }
    }

    res.send(`🤷 Who knows 🤷`);
}
