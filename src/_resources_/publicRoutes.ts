import * as express from 'express';
import rateLimit from 'express-rate-limit';
import {
    deviceAuthorisationMiddleware,
    getAuthenticate,
    getServiceAuthenticate,
    refreshToken,
} from './system/securityRoutes';
import { Router } from 'express';
import paths from '../../config/paths';
import { getGasPrice } from './transaction/transactionRoutes';

const isProd = process.env.NODE_ENV === 'PROD';

export default function getPublicRouter() {
    const router = Router();

    router.get('/favicon.ico', (_req, res) => res.end());
    router.use('/email_res', express.static(paths.EMAIL_RESOURCES));
    router.get('/ping', (_req, res) => res.status(200).end());

    const authRateLimiter = rateLimit({
        windowMs: 5_000,
        max: 5,
        standardHeaders: true,
    });

    if (isProd) {
        router.use('/auth/token', authRateLimiter);
        router.use('/service/token', authRateLimiter);
        router.use('/authorize', authRateLimiter);
    }
    router.post('/auth/token', ...getAuthenticate());
    router.post('/service/token', ...getServiceAuthenticate());
    router.get('/authorize', ...deviceAuthorisationMiddleware());

    // used to get a fresh token.
    router.post('/token/refresh', refreshToken);

    // Don't ask.
    router.get('/gas_price', getGasPrice);

    return router;
}
