import * as dayjs from 'dayjs';
import * as _ from 'lodash';
import { BudgetModelBuilder } from '../../models/BudgetModelBuilder';
import {
    Category as CategoryDto,
    Transaction as TransactionDto,
    TransactionDescriptionInfo,
} from '../../models/entities';
import { SystemCategory } from '../../models/enums';
import { TreeNode } from '../../models/nTree';
import { Category } from '../../data/categoryCollection';
import { Transaction } from '../../data/transactionCollection';
import { HydratedPlannedTransaction } from '../../data/plannedTransactionCollection';
import { BudgetModelType, CategoryInfo } from '../../models/BudgetModel';
import { esSearch } from '../../data/elasticsearch/elasticsearch';

export enum TimelineFields {
    categoryHierarchy = 'categoryHierarchy',
    plannedTransactions = 'plannedTransactions',
}

export enum TimelineEntityType {
    transaction = 'transaction',
    category = 'category',
    // TODO: To be determined
    //budgetEntry = 'budgetEntry'
}

type TimelineTransaction = {
    entity: TimelineEntityType;
    plannedAmount?: number;
    plannedDate?: string;
    isLegacy?: boolean;
    occurrences?: Array<{
        description?: string;
        descriptionInfo?: TransactionDescriptionInfo;
        amount?: number;
    }>;
} & Partial<TransactionDto>;

type TimelineCategory = {
    entity: TimelineEntityType;
    observed?: number;
    // planned?: number;
    transactionIds?: string[];
    children?: TimelineCategory[];
} & Partial<Omit<CategoryDto, 'children'>>;

export type TimelineEntity = TimelineTransaction | TimelineCategory;

type ViewBuilderParams = {
    fields?: TimelineFields[];
    granularity?: string;
};

export const timelineViewMapping = {
    [TimelineEntityType.transaction]: buildTransactionView,
    [TimelineEntityType.category]: buildCategoryView,
};

export function splitTimeSpanByYear(from: dayjs.Dayjs, to: dayjs.Dayjs) {
    let currentYear = from.year();
    const endYear = to.year();
    const periods = [];
    while (currentYear <= endYear) {
        const startOfYear = dayjs().year(currentYear).startOf('year');
        const endOfYear = dayjs().year(currentYear).endOf('year');
        periods.push({
            yearStart: from.isAfter(startOfYear) ? from.clone() : startOfYear,
            yearEnd: to.isBefore(endOfYear) ? to.clone() : endOfYear,
        });
        currentYear++;
    }
    return periods;
}

export async function prepareTimelineModel({
    userId,
    entity,
    filter,
    fields,
    search,
}: {
    userId: string;
    entity: TimelineEntityType;
    filter: Record<string, string>;
    fields: TimelineFields[];
    search?: string;
}) {
    let transactionIds = null;
    if (search) {
        let from = filter?.date_from && new Date(filter?.date_from);
        let to = filter?.date_to && new Date(filter?.date_to);
        transactionIds = await esSearch(userId, search, null, null, from, to);
    }

    const budgetModelBuilder = BudgetModelBuilder.create(
        {
            userId,
            modelYear: filter?.date_from ? dayjs(filter?.date_from).year() : dayjs().year(),
            from: filter?.date_from ? dayjs(filter?.date_from) : null,
            to: filter?.date_to ? dayjs(filter?.date_to) : null,
            filter,
            transactionIds,
        },
        { forceMultiYearModeling: true }
    );
    if (entity === TimelineEntityType.category) {
        budgetModelBuilder.appendObservedAmount();
    } else if (entity === TimelineEntityType.transaction) {
        budgetModelBuilder.appendTransactions();
    }
    if (fields?.includes(TimelineFields.plannedTransactions)) {
        budgetModelBuilder.appendPlannedAmount();
    }
    return await budgetModelBuilder.build();
}

function buildTransactionView(model: BudgetModelType, params: ViewBuilderParams) {
    const items: TimelineTransaction[] = [];
    const hasCategoryHierarchy = params.fields?.includes(TimelineFields.categoryHierarchy) || undefined;

    const hierarchyMap = model.getCategoryHierarchy((categoryInfo) => ({
        _id: categoryInfo.category._id,
        name: categoryInfo.category.name,
        hue: categoryInfo.category.hue,
        systemType: categoryInfo.category.systemType,
        type: categoryInfo.category.type,
    }));
    model.iterate((node) => {
        const { transactions, plannedTransactions, category } = node.value;

        items.push(
            ...(transactions?.map((t) => {
                const plannedTransaction = plannedTransactions?.find((pt: HydratedPlannedTransaction) =>
                    pt.matchedTransactions?.includes(t._id.toString())
                );
                return {
                    ...Transaction.toDto(t),
                    entity: TimelineEntityType.transaction,
                    categoryHierarchy: hasCategoryHierarchy && hierarchyMap.get(t.category),
                    recurring: !!plannedTransaction,
                    plannedAmount: plannedTransaction?.amount || null,
                    plannedDate: plannedTransaction?.date ? new Date(plannedTransaction?.date).toISOString() : null,
                };
            }) || [])
        );

        const pendingPlannedTransactions =
            plannedTransactions?.filter((pt: HydratedPlannedTransaction) => _.isEmpty(pt.matchedTransactions)) || [];
        items.push(
            ...pendingPlannedTransactions.map((t) => ({
                entity: TimelineEntityType.transaction,
                plannedAmount: t?.amount || null,
                plannedDate: t?.date ? new Date(t?.date).toISOString() : null,
                category: category.name,
                categoryHierarchy: hasCategoryHierarchy && hierarchyMap.get(category.name),
                isPending: true,
                recurring: true,
            }))
        );
    });

    return items;
    // return _.chain(items)
    //     .groupBy((item) => dayjs(item.date).format('YYYY-MM-DD') + item.category)
    //     .map((items) => {
    //         const [groupItem, ...legacyItems] = items;
    //         groupItem.occurrences = items.map((item) => _.pick(item, ['description', 'descriptionInfo', 'amount']));
    //         return [
    //             groupItem,
    //             ...legacyItems.map((legacyItem) => {
    //                 legacyItem.isLegacy = true;
    //                 return legacyItem;
    //             }),
    //         ];
    //     })
    //     .flatten()
    //     .value();
}

function buildCategoryView(model: BudgetModelType): Array<TimelineCategory> {
    const nonTransferRootNodes = model
        .getRootNodes()
        .filter((node) => node.value.category.systemType !== SystemCategory.transfer);
    return nonTransferRootNodes.map(buildCategorySpendingTree).filter(Boolean);
}

function buildCategorySpendingTree(node: TreeNode<CategoryInfo>): TimelineCategory {
    let { category, observedAmount } = node.value;

    if (observedAmount === 0) {
        return null;
    }

    return {
        ...Category.toDto(category),
        observed: observedAmount,
        children: node.children?.map(buildCategorySpendingTree).filter(Boolean),
        // transactionIds: transactions?.map((t) => t._id.toString()),
        entity: TimelineEntityType.category,
    };
}
