import * as service from '../../services/service';
import * as secretUtils from '../../utils/secretUtils';
import { HydratedUser } from '../../data/userCollection';
import { LocationContext } from '../../utils/queryUtil';
import * as config from 'config';

type LocationSecurityCheckParameters = {
    locationContext?: LocationContext;
    user?: HydratedUser;
    hashedKey?: string;
};

export async function doLocationSecurityCheck({ user, locationContext }: LocationSecurityCheckParameters) {
    const existingLocation = user.getLocation(locationContext.ip);
    const isNewLocation = !existingLocation;
    if (isNewLocation) {
        locationContext.location.ip = locationContext.ip;
        locationContext.location.timestamp = Date.now();
        user.locations.push(locationContext.location);
    } else {
        existingLocation.timestamp = Date.now();
    }
    user.markModified('locations');

    let device = locationContext.device;
    const existingDevice = device.deviceToken && user.getDevice(device.deviceToken);
    const isNewDevice = !existingDevice;
    if (isNewDevice) {
        device.lastLocation = locationContext.locationLabel;
        device.timestamp = Date.now();
        device.deviceToken = secretUtils.sign({ sub: user.googleUserId }, '3y');

        user.devices.push(device);
        user.markModified('devices');
        await user.save();
        device = user.devices.find((dev) => dev.deviceToken === device.deviceToken);
        locationContext.device = device;

        if (process.env.NODE_ENV === 'PROD') {
            const token = secretUtils.sign({ sub: user.googleUserId, deviceId: device._id.toString() }, '1h');
            await service.mailer.sendNewDeviceActivity({
                from: config.get('SYSTEM_EMAILS.ADMIN'),
                to: user.email,
                apiAddress: config.get('API_ADDRESS'),
                isDesktop: false, //device.isDesktop,
                device: `${device.platform} - ${device.manufacturer} ${device.version}`,
                location: locationContext.locationLabel,
                authorizeToken: token,
            });
        } else {
            device.verified = true;
            user.markModified('devices');
            await user.save();
        }
    } else {
        existingDevice.lastLocation = locationContext.locationLabel;
        existingDevice.ip = locationContext.ip;
        existingDevice.timestamp = Date.now();
        existingDevice.version = device.version;

        user.markModified('devices');
        await user.save();
    }
}

export async function doServiceLocationSecurityCheck({
    locationContext,
    user,
    hashedKey,
}: LocationSecurityCheckParameters) {
    const existingDevice = user.getDevice(hashedKey);
    const isNewDevice = !existingDevice;
    let device = locationContext.device;
    if (isNewDevice) {
        device.verified = true; // services bypass device verification.
        device.lastLocation = locationContext.locationLabel;
        device.timestamp = Date.now();

        device.deviceToken = hashedKey;
        user.devices.push(device);
        device = user.devices.find((dev) => dev.deviceToken === device.deviceToken);
        locationContext.device = device;
    } else {
        existingDevice.lastLocation = locationContext.locationLabel;
        existingDevice.timestamp = Date.now();
    }
    user.markModified('devices');
    await user.save();
}
