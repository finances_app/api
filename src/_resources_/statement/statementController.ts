import { StatementFileType } from '../../models/enums';
import { updatePlannedTransactions } from '../budget/budgetController';
import { HydratedStatement, Statement } from '../../data/statementCollection';
import { Transaction } from '../../data/transactionCollection';
import * as FileDao from '../../data/fileCollection';
import { EssentialTransactionData } from '../../models/entities';
import { upsertEsTransactions } from '../../data/elasticsearch/elasticsearch';

export async function createStatementWithTransactions({
    userId,
    transactions,
    date,
    fileType,
    fileId,
}: {
    userId: string;
    transactions: EssentialTransactionData[];
    date: Date;
    fileType: StatementFileType;
    fileId: string;
}) {
    let statement: HydratedStatement;
    try {
        let statements = await Statement.insertMany([
            {
                date,
                fileType,
                fileId,
                userId,
            },
        ]);
        statement = statements[0];

        const hydratedTransactions = await Transaction.insertMany(
            transactions.map((t) => ({
                ...t,
                statementId: statement._id,
                userId,
                creationDate: undefined,
            }))
        );
        await updatePlannedTransactions(userId, hydratedTransactions);

        await upsertEsTransactions(
            hydratedTransactions.map((t) => ({
                ...t.toEsEntity(),
                accountType: fileType,
            }))
        );
    } catch (err) {
        if (statement) {
            await Transaction.remove({ statementId: statement._id });
            await Statement.findByIdAndRemove(statement._id);
        }
        throw err;
    }

    return statement;
}

export async function updateApprovedStatus({ userId, statementId }: { userId: string; statementId: string }) {
    let statement = await Statement.findOne({
        _id: statementId,
    }).forUser<HydratedStatement>(userId);
    // Auto approve the statement if all transactions are assigned to a category.
    if (statement.fileType === StatementFileType.webScraper) {
        let transactions = await Transaction.find({
            statementId: statement.id,
        })
            .forUser(userId)
            .select('category')
            .lean();
        statement.isApproved = transactions.every((t) => t.category);
        await statement.save();
    }
}

export async function findOrCreateStatement(
    userId: string,
    firstOfMonth: Date,
    statementType: StatementFileType
): Promise<HydratedStatement> {
    let statement = await Statement.findOne({
        date: firstOfMonth,
        fileType: statementType,
    }).forUser<HydratedStatement>(userId);
    if (!statement) {
        statement = new Statement({
            userId,
            date: firstOfMonth,
            fileType: statementType,
        });
        await statement.save();
    }
    return statement;
}

export async function removeEmptyStatements(statementIds: string[]) {
    for (let statementId of statementIds) {
        // this code doesnt work, FIXME
        const transactionCount = await Transaction.countDocuments({ statementId });
        if (transactionCount === 0) {
            const deletedStatement = await Statement.findByIdAndRemove(statementId);
            if (deletedStatement.fileId) {
                await FileDao.remove(deletedStatement.fileId);
            }
        }
    }
}
