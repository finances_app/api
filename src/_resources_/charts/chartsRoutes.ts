/**
 * Created by hugo on 2020-02-07
 */
import * as _ from 'lodash';
import * as dayjs from 'dayjs';
import { Dayjs } from 'dayjs';
import NTree from '../../models/nTree';
import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler, toObject } from '../../utils/queryUtil';
import { ClientError } from '../../utils/errors';
import { Transaction } from '../../data/transactionCollection';
import { Category } from '../../data/categoryCollection';
import { SystemCategory, isExpenseSystemCategory, isBaseTransfer } from '../../models/enums';
import { Account } from '../../data/accountCollection';
import { BudgetModelBuilder } from '../../models/BudgetModelBuilder';
import { SimpleDateInterval, splitDateRangeByYears } from '../../utils/dateUtil';
import { Budget } from '../../data/budgetCollection';
import { BudgetModel } from '../../models/BudgetModel';
import { HydratedPlannedTransaction } from '../../data/plannedTransactionCollection';
import { VirtualPlannedTransaction } from '../../data/budgetEntryCollection';
import { BalanceHistoryDataset } from '../../models/entities';
import { isEmpty } from 'lodash';

const DEFAULT_PLOT_DATA_GRANULARITY = 'week';

export default function getChartsRouter() {
    const router = makeAuthenticatedRouter();

    router.route('/bar/:from_date/:to_date/:group_period').get(makeRequestHandler(getBarChart));
    router.route('/line/:from_date/to/:to_date').get(makeRequestHandler(lineChart));
    router.route('/pocket_money').get(makeRequestHandler(getPocketMoney));

    return router;
}

type BarChartValue = {
    name: string;
    color: string;
    data: BalanceHistoryDataset;
};

/**
 * Get data for a stacked bar chart of spending / {period} of every spending and revenus
 */
// GET  /api/charts/bar/:from_date/:to_date/:group_period
async function getBarChart(req: AuthenticatedRequest) {
    let params = req.params;

    const acceptedPeriods = ['M', 't', 'y'];
    if (!acceptedPeriods.includes(params.group_period)) {
        throw new ClientError(`group_period value is not valid. Accepted values are: [${acceptedPeriods.join(', ')}]`);
    }

    let fromDate = new Date(params.from_date);
    let toDate = new Date(params.to_date);

    let categoryAggregates = await Transaction.aggregateAmountByCategoryAndPeriod(
        req.userId,
        fromDate,
        toDate,
        params.group_period as SimpleDateInterval
    );

    let groupedCategories = _.groupBy(categoryAggregates, (_agg) => {
        let groupDate = dayjs().set('year', _agg._id.year).startOf('year');
        if (params.group_period === 't') {
            groupDate = groupDate.set('month', _agg._id.t * 4).startOf('month');
        } else if (params.group_period === 'M') {
            groupDate = groupDate.set('month', _agg._id.M).startOf('month');
        }
        return groupDate.toDate().getTime();
    });

    let categories = await Category.find().forUser(req.userId).lean();
    let categoryTree = new NTree(
        categories,
        (c) => c.name,
        (c) => c.parent
    );
    let categoryRootMap = categoryTree.mapLeafsToRoots((_cat) => _cat.name);

    let series: BarChartValue[] = [];
    for (let [groupDate, values] of Object.entries(groupedCategories)) {
        let rootCategorySums = new Map();
        for (let categoryAggregate of values) {
            let _cat = categories.find((_cat) => _cat.name === categoryAggregate._id.category);
            if (!_cat || _cat.systemType === SystemCategory.transfer) {
                continue;
            }

            let rootCategoryName = categoryRootMap.get(categoryAggregate._id.category);
            let sum = rootCategorySums.get(rootCategoryName) || 0;
            sum += categoryAggregate.amount;
            rootCategorySums.set(rootCategoryName, sum);
        }

        for (let [rootName, sum] of rootCategorySums.entries()) {
            let rootCategory = categories.find((_cat) => _cat.name === rootName);
            let s = series.find((_s) => _s.name === rootCategory.name);

            if (s) {
                s.data.push([parseInt(groupDate), Math.round(sum * 100) / 100]);
            } else {
                s = {
                    name: rootCategory.name,
                    color: rootCategory.hue,
                    data: [[parseInt(groupDate), Math.round(sum * 100) / 100]],
                };
                series.push(s);
            }
        }
    }

    return { items: series };
}

// GET  /api/charts/line/:from_date/to/:to_date
async function lineChart(req: AuthenticatedRequest) {
    // TODO: This new route will still ignore planned transactions in the past if the current date is close to the
    //       start of the year. This is because the budget model is hard scoped to a single year.

    const from = dayjs(req.params.from_date);
    const to = dayjs(req.params.to_date);
    // possible values: "planned", "plannedAdjusted"
    const fields = toObject(req.query.fields.toString()) || [];
    const granularity = req.query.granularity?.toString() || DEFAULT_PLOT_DATA_GRANULARITY;

    const accounts = await Account.getActiveAccounts(req.userId);
    const balanceOffset = _.sumBy(accounts, (_a) => _a.amount);
    const lastUpdateTimestamp = _.chain(accounts)
        .map((account) => new Date(account.updatedAt).getTime())
        .sortBy((timestamp) => timestamp)
        .last()
        .value();

    const defaultFrom = dayjs().startOf('year');
    const _from = from.isBefore(defaultFrom) ? defaultFrom : from;

    let model = await BudgetModelBuilder.create({
        userId: req.userId,
        modelYear: dayjs().year(),
        from: _from,
        to,
    })
        .appendTransactions()
        .appendPlannedAmount()
        .build();
    const pendingPlannedTransactions = await findPendingPlannedTransactions(model, lastUpdateTimestamp);
    const pendingPlannedTransactionsSum = _.sumBy(pendingPlannedTransactions, (t) => t.amount);

    // The only way to know the balance at a specific date is to calculate it from the current balance.
    // If the year is the current one, just get teh current balance and offset the values by this amount.
    let historicData = from.isAfter(dayjs()) ? [] : await getBalanceHistoryUpTo(req.userId, from, balanceOffset);
    if (historicData.length > 0 && _.last(historicData)[0] < lastUpdateTimestamp) {
        historicData.push([lastUpdateTimestamp, balanceOffset]);
    }
    if (to.isBefore(dayjs())) {
        const endOfRange = to.clone().endOf('date').valueOf();
        historicData = historicData.filter((_tuple) => _tuple[0] <= endOfRange);
    }

    let plannedData: BalanceHistoryDataset = [];
    if (fields.includes('planned')) {
        let newPlannedData = model.buildPlannedPlotData(granularity);
        const startOfYearBalance = findStartOfYearBalance(_from.year(), historicData);
        newPlannedData.forEach((tuple) => {
            tuple[1] += startOfYearBalance;
        });
        plannedData.push(...newPlannedData);
    }

    let plannedAdjustedData: BalanceHistoryDataset;
    if (fields.includes('plannedAdjusted')) {
        const endOfYear = dayjs().endOf('year');
        model = await BudgetModelBuilder.create({
            userId: req.userId,
            modelYear: dayjs().year(),
            from: dayjs(lastUpdateTimestamp),
            to: to.isBefore(endOfYear) ? to : endOfYear,
        })
            .appendTransactions()
            .appendPlannedAmount()
            .build();
        plannedAdjustedData = await model.buildPlannedAdjustedPlotData(granularity);
        plannedAdjustedData.forEach((tuple) => {
            tuple[1] += balanceOffset + pendingPlannedTransactionsSum;
        });
    }

    return {
        observed: prepareBalanceDataset(historicData),
        planned: prepareBalanceDataset(plannedData),
        plannedAdjusted: prepareBalanceDataset(plannedAdjustedData),
    };
}

async function findPendingPlannedTransactions(model: BudgetModel, lastUpdateTimestamp: number) {
    const pendingPlannedTransactions: Array<VirtualPlannedTransaction | HydratedPlannedTransaction> = [];
    model.iterate((node) => {
        const { plannedTransactions } = node.value;
        const filteredPlannedTransactions =
            plannedTransactions?.filter(
                (pt) =>
                    _.isEmpty((pt as HydratedPlannedTransaction).matchedTransactions) &&
                    new Date(pt.date).getTime() <= lastUpdateTimestamp
            ) || [];
        pendingPlannedTransactions.push(...filteredPlannedTransactions);
    });
    return pendingPlannedTransactions;
}

async function getBalanceHistoryUpTo(
    userId: string,
    start: Dayjs,
    balanceOffset: number
): Promise<Array<[number, number]>> {
    const intervals = splitDateRangeByYears(start, dayjs().endOf('year'));
    let transactionAmounts = [];

    for (const { from, to } of intervals) {
        const budget = await Budget.getClosest(userId, from.year());
        let excludedCategoryNames = [];
        if (budget?.isClosed) {
            excludedCategoryNames = budget.categoriesSnapshot
                .filter((category) => isBaseTransfer(category))
                .map((category) => category.name);
        } else {
            excludedCategoryNames = await Category.getTransferCategoryNames(userId);
        }

        let _transactionAmounts = await Transaction.getBalanceHistory(
            userId,
            from.toDate(),
            to.toDate(),
            excludedCategoryNames
        );
        transactionAmounts.push(..._transactionAmounts);
    }

    return transactionAmounts
        .reverse()
        .reduce((acc, val, index) => {
            let date = new Date(val._id.year, val._id.month - 1, val._id.day).getTime();
            if (index === 0) {
                acc.push([date, parseFloat((balanceOffset - val.amount).toFixed(2))]);
            } else {
                acc.push([date, parseFloat((acc[index - 1][1] - val.amount).toFixed(2))]);
            }
            return acc;
        }, [])
        .reverse();
}

function prepareBalanceDataset(dataset: BalanceHistoryDataset) {
    return dataset.map((_tuple) => {
        _tuple[1] = Math.round(_tuple[1] * 100) / 100;
        return _tuple;
    });
}

function findStartOfYearBalance(year: number, dataset: BalanceHistoryDataset) {
    if (isEmpty(dataset)) {
        return 0;
    }

    const endOfLastYear = dayjs().year(year).subtract(1, 'year').endOf('year');
    let endOfLastYearIndex = dataset.length - 1;
    let datePointer = dayjs(dataset[endOfLastYearIndex][0]);

    // Find the data point index for the end of last year
    while (datePointer.isAfter(endOfLastYear, 'date') && endOfLastYearIndex > 0) {
        endOfLastYearIndex--;
        datePointer = dayjs(dataset[endOfLastYearIndex][0]);
    }
    return dataset[Math.max(endOfLastYearIndex, 0)][1];
}

async function getPocketMoney(req: AuthenticatedRequest) {
    // The budget is calculated from planned revenues minus the combined planned total of a provided list of root categories.
    // I think it should be a rolling 7 days calculation ?
    //
    const reservedBudgetCategories = [
        '5e6898596409847178432de1', // essentials
        '5e6898126409847178432dde', // security
    ];
    const leftoversCategories = [
        '5e68982c6409847178432ddf', // projects
    ];

    let model = await BudgetModelBuilder.create({
        userId: req.userId,
        modelYear: dayjs().year(),
    })
        .appendObservedAmount(dayjs().subtract(7, 'day').startOf('day'))
        .appendPlannedAmount()
        .build();

    const yearlyBudget = _.chain(model.getRootNodes())
        .filter((node) => node.value.category.systemType === SystemCategory.income)
        .sumBy((node) => node.value.plannedAmount)
        .value();
    const [reservedNodes, otherNodes] = _.chain(model.getRootNodes())
        .filter((node) => isExpenseSystemCategory(node.value.category.systemType))
        .partition((node) => reservedBudgetCategories.includes(node.value.category._id.toString()))
        .value();
    const reservedBudget = _.sumBy(reservedNodes, (node) => node.value.plannedAmount);

    const [leftoverNodes, unplannedNodes] = _.partition(otherNodes, (node) =>
        leftoversCategories.includes(node.value.category._id.toString())
    );
    const plannedAmount = _.sumBy(unplannedNodes, (node) => node.value.plannedAmount) / 52;
    const recurringAmount =
        _.chain(unplannedNodes)
            .map((node) => node.getLeaves())
            .flatten()
            .filter((value) => value.budgetEntry?.isComplex)
            .sumBy((value) => value.plannedAmount)
            .value() / 52;

    const unplannedAmount = model.getUnplannedObservedAmount();

    const disposableWeeklyIncome = (yearlyBudget - reservedBudget) / 52;
    const leftoverBudget = disposableWeeklyIncome - plannedAmount - Math.abs(unplannedAmount);
    return {
        max: Math.max(disposableWeeklyIncome, plannedAmount + Math.abs(unplannedAmount)),
        series: _.compact([
            leftoverBudget > 0 && {
                name: 'Budget',
                data: [Math.round(leftoverBudget * 100) / 100],
                color: '#888',
            },
            {
                name: 'Non planifié',
                data: [Math.round(Math.abs(unplannedAmount) * 100) / 100],
                color: '#ef83b0',
            },
            {
                name: 'Réservé',
                data: [Math.round((plannedAmount - recurringAmount) * 100) / 100],
            },
            {
                name: 'Récurant',
                data: [Math.round(recurringAmount * 100) / 100],
                color: '#F2A25C',
            },
        ]),
    };
}
