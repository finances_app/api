import { Router } from 'express';
import rateLimit from 'express-rate-limit';
import getTimelineRouter from './timeline/timelineRoutes';
import getTransactionRouter from './transaction/transactionRoutes';
import getStatementRouter from './statement/statementRoutes';
import getCategoryRouter from './category/categoryRoutes';
import getAccountRouter from './account/accountRoutes';
import getChartsRouter from './charts/chartsRoutes';
import getBudgetRouter from './budget/budgetRoutes';
import getGoalRouter from './goal/goalRoutes';
import getUserRouter from './user/userRoutes';
import getAttachmentRouter from './system/attachmentRoutes';
import getExtractRouter from './transaction/extractRoutes';

const isProd = process.env.NODE_ENV === 'PROD';

export default function getApiRouter() {
    const router = Router();

    const apiRateLimiter = rateLimit({
        windowMs: 60_000,
        max: 150,
    });

    if (isProd) {
        router.use(apiRateLimiter);
    }

    router.use('/api/timeline', getTimelineRouter());
    router.use('/api/transaction', getTransactionRouter());
    router.use('/api/statement', getStatementRouter());
    router.use('/api/category', getCategoryRouter());
    router.use('/api/account', getAccountRouter());
    router.use('/api/charts', getChartsRouter());
    router.use('/api/budget', getBudgetRouter());
    router.use('/api/goal', getGoalRouter());
    router.use('/api/user', getUserRouter());
    router.use('/api/attachment', getAttachmentRouter());
    router.use('/api/extract', getExtractRouter());

    return router;
}
