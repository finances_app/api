import * as _ from 'lodash';
import { Category, HydratedCategory, LeanCategory } from '../../data/categoryCollection';
import { CategoryType, SystemCategory } from '../../models/enums';
import { EssentialTransactionData, Category as CategoryDto } from '../../models/entities';
import { ClientError } from '../../utils/errors';
import { Budget, LeanBudget } from '../../data/budgetCollection';
import * as dayjs from 'dayjs';
import { HydratedTransaction, Transaction } from '../../data/transactionCollection';
import { FilterQuery } from 'mongoose';
import { HydratedStatement, Statement } from '../../data/statementCollection';
import { Dayjs } from 'dayjs';
import { prepareDescription } from '../../utils/transactionUtils';

export async function findAndCreateNewCategories(userId: string, transactions: EssentialTransactionData[]) {
    const distinctCategories = _.chain(transactions)
        .map((t) => t.category)
        .filter(Boolean)
        .uniq()
        .value();
    for (const category of distinctCategories) {
        const matchingTransactions = transactions.filter((t) => t.category === category);
        await Category.createIfNotExistsOrUpdate(
            userId,
            CategoryType.base,
            category,
            _.first(matchingTransactions).amount > 0 ? SystemCategory.income : SystemCategory.expense,
            _.chain(matchingTransactions)
                .map((t) => t.description)
                .uniq()
                .value()
        );
    }
}

export async function updateCategoryAndReferences(userId: string, categoryDto: CategoryDto) {
    let existingCategory = await Category.findOne({
        name: categoryDto.name,
    })
        .forUser<LeanCategory>(userId)
        .lean();
    if (existingCategory && existingCategory._id.toString() !== categoryDto.id) {
        throw new ClientError(`Il y a déjà une catégorie avec le nom '${categoryDto.name}'`);
    }

    // The system type is inherited from the parent. A validation is done client side to communicate that.
    if (categoryDto.parent) {
        const parentCategory = await Category.findOne({ name: categoryDto.parent })
            .forUser<LeanCategory>(userId)
            .lean();
        categoryDto.systemType = parentCategory.systemType;
    }

    const activeBudget = await Budget.findOne({ categoriesSnapshot: [] })
        .forUser<LeanBudget>(userId)
        .select('year')
        .lean();

    let oldCategory = await Category.findById(categoryDto.id);
    let removedChildren: string[], updatedCategory: HydratedCategory;
    if (oldCategory) {
        if (oldCategory.userId !== userId) {
            throw new ClientError('', 403);
        }

        removedChildren = await removeReferencesAsParent(userId, oldCategory.name);
        if (oldCategory.type === CategoryType.base) {
            const filterQuery: FilterQuery<HydratedTransaction> = {
                userId: userId,
                category: oldCategory.name,
            };
            if (activeBudget) {
                filterQuery['year'] = {
                    $gte: dayjs().year(activeBudget.year).startOf('year'),
                    $lte: dayjs().year(activeBudget.year).endOf('year'),
                };
            }
            await Transaction.updateMany(filterQuery, { $set: { category: categoryDto.name } });
        }

        categoryDto.matchedDescriptions = oldCategory.matchedDescriptions;
        updatedCategory = await Category.findByIdAndUpdate(categoryDto.id, categoryDto);
    } else {
        let category = new Category(categoryDto);
        category.userId = userId;
        await category.save();
        updatedCategory = category;
    }

    await addReferencesToChildren(userId, categoryDto, categoryDto.children || removedChildren);

    return updatedCategory;
}

export async function deleteCategoryAndReferences(userId: string, category: HydratedCategory) {
    const activeBudget = await Budget.getActive(userId);

    if (activeBudget) {
        let budgetEntry = activeBudget.findEntryByCategory(category.id);
        if (budgetEntry) {
            budgetEntry.remove();
            await activeBudget.save();
        }
    }

    if (category.type === CategoryType.base) {
        let filterQuery: FilterQuery<HydratedStatement> = {
            userId: userId,
            category: category.name,
        };
        if (activeBudget) {
            filterQuery['date'] = {
                $gte: dayjs().year(activeBudget.year).startOf('year').toDate(),
                $lte: dayjs().year(activeBudget.year).endOf('year').toDate(),
            };
        }
        let referencedStatementIds = await Transaction.find(filterQuery).select('statementId').lean();

        await Statement.updateMany(
            {
                userId: userId,
                _id: _.chain(referencedStatementIds)
                    .map((t) => t.statementId)
                    .uniqBy((_id) => _id.toString())
                    .value(),
            },
            { $set: { isApproved: false } }
        );

        await Transaction.updateMany(filterQuery, { $set: { category: null } });

        await category.remove();
    } else {
        await removeReferencesAsParent(userId, category.name);
        await category.remove();
    }
}

async function addReferencesToChildren(userId: string, category: CategoryDto, children: string[]) {
    await Category.updateMany(
        { userId, name: { $in: children } },
        { $set: { parent: category.name, systemType: category.systemType } }
    );
    const subCategory = await Category.findSubCategories(userId, category.name);
    await Category.updateMany(
        { userId, name: { $in: subCategory.map((c) => c.name).filter((c) => !children.includes(c)) } },
        { $set: { systemType: category.systemType } }
    );
}

async function removeReferencesAsParent(userId: string, categoryName: string) {
    let subCategories = await Category.find({
        userId,
        parent: categoryName,
    })
        .select('_id name')
        .lean();
    await Category.updateMany({ _id: { $in: subCategories.map((_cat) => _cat._id) } }, { $set: { parent: null } });
    return subCategories.map((_c) => _c.name);
}

/**
 * @deprecated
 */
export async function assignCategories<T extends EssentialTransactionData>(userId: string, transactions: T[]) {
    const distinctDescriptions = _.chain(transactions)
        .map((transaction) => transaction.description)
        .uniq()
        .value();
    const subCategories = await Category.find({
        matchedDescriptions: { $ne: null, $exists: true },
    })
        .ofBaseType()
        .forUser(userId);
    for (let description of distinctDescriptions) {
        const subCategory = subCategories.find((category) =>
            category.matchedDescriptions.some((match) => prepareDescription(match) === prepareDescription(description))
        );
        if (subCategory) {
            for (let transaction of transactions.filter((t) => t.description === description)) {
                transaction.category = subCategory.name;
            }
        }
    }

    // TODO: some day, this es auto-assignation code should get fixed.
    // let results = await esDao.assignRelevantCategories(userId, distinctDescriptions, fileType);
    //
    // let categories = await this.model('category').find().forUser(userId).select('name').lean();
    //
    // // assign category to transactions
    // for (let transaction of transactions) {
    //     let categoryMatches = results.filter(
    //         (_result) => _result.type === transaction.type && _result.description === transaction.description
    //     );
    //
    //     let category =
    //         !_.isEmpty(categoryMatches) &&
    //         categories.find((_cat) => _cat.name.toLowerCase() === _.first(categoryMatches).category);
    //     if (
    //         category &&
    //         (_.first(categoryMatches).score >= importConfig.category_es_score_threshold || categoryMatches.length === 1)
    //     ) {
    //         transaction.category = category.name;
    //     }
    // }

    return transactions;
}

export async function iterateTransactionsWithMatchingCategory<T extends EssentialTransactionData>(
    userId: string,
    transactions: T[],
    iteratee: (transaction: T, category: HydratedCategory) => void
) {
    const distinctDescriptions = _.chain(transactions)
        .map((transaction) => transaction.description)
        .uniq()
        .value();
    const subCategories = await Category.find({
        matchedDescriptions: { $ne: null, $exists: true },
    })
        .ofBaseType()
        .forUser(userId);
    for (let description of distinctDescriptions) {
        const subCategory = subCategories.find((category) =>
            category.matchedDescriptions.some((match) => prepareDescription(match) === prepareDescription(description))
        );
        if (subCategory) {
            for (let transaction of transactions.filter((t) => t.description === description)) {
                iteratee(transaction, subCategory);
            }
        }
    }
}

export async function getLastActivity(userId: string, from: Dayjs, to: Dayjs, categories?: Array<LeanCategory>) {
    let baseCategories = categories || (await Category.find().ofBaseType().forUser(userId).select('name').lean());
    let recentTransactions = await Transaction.find({
        category: {
            $exists: true,
            $nin: [null, ''],
        },
        date: {
            $gte: (from || dayjs().subtract(1, 'year')).toDate(),
            $lte: (to || dayjs()).toDate(),
        },
    })
        .forUser(userId)
        .sort('-date')
        .select('category date')
        .lean();

    let lastActivityMap: Record<string, Date> = {};
    let visitedCategories: string[] = [];
    for (let recentTransaction of recentTransactions) {
        let _transaction = recentTransaction;
        if (!visitedCategories.includes(_transaction.category)) {
            visitedCategories.push(_transaction.category);
            let category = baseCategories.find((_cat) => _cat.name === _transaction.category);
            // Need to make sure the category still exists (we can hit a transaction of last year that was matched
            // to a category that was deleted for this year's budget)
            if (category) {
                lastActivityMap[category._id.toString()] = _transaction.date;
            }
        }
    }

    return lastActivityMap;
}
