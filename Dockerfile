FROM node:lts-alpine
EXPOSE 80
EXPOSE 443

# HEALTHCHECK --interval=10s --timeout=2s --start-period=15s CMD curl --fail localhost:8092 || exit 1

# tools for mongo backups
RUN apk add mongodb-tools

# Timezone tool
RUN apk add tzdata
ENV TZ=America/Toronto
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir /lib/finances_server
WORKDIR /lib/finances_server

# explicitely copy the config first to trigger a yarn install only when needed
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn

COPY . .

CMD ["yarn", "start"]
