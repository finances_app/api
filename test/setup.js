/**
 * Requirements for the test suite:
 * - Should run from an empty db (only containing a test user profile)
 * - No mocked server code
 * - test data reset by clearing the mongo db directly
 */

const superagent = require('superagent');
const { expect } = require('chai');
const mongoose = require('mongoose');
const testUtils = require('./utils/tests');

global.expect = expect;

describe('Sanity check', function () {
    before(async function () {
        mongoose.connect(process.env.MONGODB_HOST + process.env.MONGODB_NAME, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        await new Promise((resolve, reject) => {
            console.log(`Connecting to database..`);
            mongoose.connection.on('error', (err) => reject(err));
            mongoose.connection.once('open', () => resolve());
        });
        await testUtils.resetDb();
    });

    it('Should be able to ping the api', async function () {
        const res = await superagent.get(process.env.API_HOSTNAME + '/ping').timeout(20000);
        expect(res.status).to.equal(200);
    });
});
