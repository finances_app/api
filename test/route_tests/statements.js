const testUtils = require('../utils/tests');
const { generateTransactions } = require('../utils/transactionGenerator');

require('../setup');

describe.skip('Statements', async function() {
    let authRes;
    before(async function () {
        authRes = await testUtils.authenticateRandomNewUser();
    });

    // test the following routes:
    // TODO: this route is critical, but can't be tested without a pdf statement.. Let's find a way !
    // PUT /api/statement
    // POST /api/extract/diff/:statement_type
});