const testUtils = require('../utils/tests');
const { generateTransactions } = require('../utils/transactionGenerator');

require('../setup');

describe.skip('Categories', async function() {
    let authRes;
    before(async function () {
        authRes = await testUtils.authenticateRandomNewUser();

        // import some transactions to categorise
    });

    // test the following routes:
    // GET PUT /api/category
    // GET /api/category/last_activity
    // GET /api/category/suggest_match/:transaction_id
    // DELETE /api/category/:category_id
    // PUT /api/transaction/assign_category/:transaction_id
});