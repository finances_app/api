const mongoose = require('mongoose');
const superagent = require('superagent');
const config = require('../testConfig.json');
const { spawn } = require('child_process');
const path = require('path');
const crypto = require('crypto');

class testUtils {
    static async resetDb() {
        console.log('restoring database..');

        const mongodb = mongoose.connection.db;
        await mongodb.dropDatabase();
        await this._mongoRestore();
    }

    static async _mongoRestore() {
        return new Promise((resolve, reject) => {
            let backupProcess = spawn(`mongorestore`, [
                `--uri`,
                process.env.MONGODB_HOST,
                path.join(__dirname, '..', '..', `./gitlab-ci/ci_data/`),
            ]);
            backupProcess.on('error', (err) => {
                reject(err);
            });
            backupProcess.on('exit', (code, signal) => {
                let res = code === null ? signal : code;
                if (code && code !== 0) {
                    reject(new Error(`mongorestore process exited with unexpected code ${code || signal}`));
                    return;
                } else if (signal) {
                    reject(new Error('mongorestore process was terminated by signal: ' + signal));
                    return;
                }
                console.log(`mongorestore complete !`);
                resolve();
            });
        });
    }

    static async authenticate() {
        const res = await superagent.post(process.env.API_HOSTNAME + '/auth/token').send({
            id_token: config.CI_USER_PASSWORD,
            user_id: config.CI_USER_ID,
            device: {
                deviceToken: config.CI_USER_DEVICE_TOKEN,
            },
            type: 'google',
        });
        return res.body;
    }

    static async authenticateRandomNewUser() {
        const userId = crypto.randomBytes(16).toString('base64');
        const res = await superagent.post(process.env.API_HOSTNAME + '/auth/token').send({
            id_token: config.CI_USER_PASSWORD,
            user_id: userId,
            type: 'google',
        });
        return {
            ...res.body,
            userId,
        };
    }

    static async apiGet(accessToken, config) {
        return superagent
            .get(process.env.API_HOSTNAME + config.url)
            .ok((res) => !!res.status)
            .set('Authorization', `Bearer ${accessToken}`);
    }

    static async apiPost(accessToken, config) {
        return superagent
            .post(process.env.API_HOSTNAME + config.url)
            .ok((res) => !!res.status)
            .send(config.data)
            .set('Authorization', `Bearer ${accessToken}`);
    }

    static async apiPut(accessToken, config) {
        return superagent
            .put(process.env.API_HOSTNAME + config.url)
            .ok((res) => !!res.status)
            .send(config.data)
            .set('Authorization', `Bearer ${accessToken}`);
    }

    static async apiDelete(accessToken, config) {
        return superagent
            .delete(process.env.API_HOSTNAME + config.url)
            .ok((res) => !!res.status)
            .set('Authorization', `Bearer ${accessToken}`);
    }
}
module.exports = testUtils;
